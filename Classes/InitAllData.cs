﻿using System;
using CargoBot.Models;
using CargoBot.Repositories;

namespace CargoBot.Classes
{
    public class InitAllData
    {
        private UnitOfWork g_UnitOfWork = new UnitOfWork(new CargoBotContext());

        public bool initData(out string _sError)
        {

            bool _bSuccess = AppUserType.initData(g_UnitOfWork);
            _bSuccess = BusinessAdminStatus.initData(g_UnitOfWork);
            _bSuccess = BusinessStatus.initData(g_UnitOfWork);
            _bSuccess = DynamicListType.initData(g_UnitOfWork);
            _bSuccess = MessageType.initData(g_UnitOfWork);
            _bSuccess = ParcelStatus.initData(g_UnitOfWork);
            _bSuccess = ResponseMessageTemplateStatus.initData(g_UnitOfWork);
            _bSuccess = AppProcess.initProcessAndSubProcessData(g_UnitOfWork);
            TripStatus.initData(g_UnitOfWork);
            AppUser.setSuperAdmins();
            _sError = string.Empty;
            int _iMessageTemplateID = 0, _iStaticMessageOption = 0, _iProcessTemplateID = 0, _iMessageID = 0;
            bool _bRet = new TmplDataWelcome().createTemplate(ref _iMessageTemplateID, ref _iStaticMessageOption, ref _iProcessTemplateID, out string _sCurrError, ref _iMessageID);
            if (!new TmplDataRegisterVehicle().createTemplate(ref _iMessageTemplateID, ref _iStaticMessageOption, ref _iProcessTemplateID, out _sCurrError, ref _iMessageID) && _bRet)
            {
                _bRet = false;
            }
            _sError = string.Concat(_sError, " >> ", _sCurrError);
            return _bRet;
        }
    }
}