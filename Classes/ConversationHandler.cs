﻿using CargoBot.Models;
using CargoBot.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.WebPages;
using Twilio;
using Twilio.AspNet.Common;
using Twilio.Rest.Api.V2010.Account;
using Twilio.TwiML;
using Twilio.Types;

namespace CargoBot.Classes {
    public class ConversationHandler {
        private UnitOfWork g_UnitOfWork = new UnitOfWork( new CargoBotContext() );
        public string g_sAppUserNumber { get; set; }
        public string g_sMessageReceived { get; set; }
        public string g_sMessageSent { get; set; }
        public bool g_bMessageReceivedValid { get; set; }
        public double g_dMessageSentCost { get; set; }
        public int g_iAppUserTypeID { get; set; }
        public int g_iResponseProcessID { get; set; }
        public int g_iResponseSubProcessID { get; set; }
        public bool g_bSimulating { get; set; }

        //public int g_iNextProcessID { get; set; }
        //public int g_iNextSubProcessID { get; set; }

        public Message g_PreviousMessage { get; set; }
        public SmsRequest g_IncomingMessage { get; set; }
        public ResponseMessageTemplate g_PreviousMessageTemplate { get; set; }
        public ResponseMessageTemplate g_ResponseMessageTemplate { get; set; }
        public IEnumerable<ResponseMessageTemplate> g_OnSuccessOthersResponseMessageTemplates { get; set; } //To be used when messaging others like when driver updates location and when a parcel is added and sender is also given a message with parcel details
        public Parcel g_Parcel { get; set; }
        public Trip g_Trip { get; set; }
        public AppUser g_AppUser { get; set; }

        public ConversationHandler( SmsRequest _IncomingMessage, bool _bSimulating ) {
            this.g_bSimulating = _bSimulating;
            this.g_IncomingMessage = _IncomingMessage;
            this.g_sAppUserNumber = _IncomingMessage.From.Replace( "whatsapp:", "" );
            this.g_sMessageReceived = _IncomingMessage.Body;
            this.g_AppUser = g_UnitOfWork.AppUsers.getFiltered( _Rec => _Rec.Phone == this.g_sAppUserNumber ).FirstOrDefault();
            if ( this.g_AppUser != null ) {
                this.g_PreviousMessage = g_UnitOfWork.ConversationLogs.getFiltered(
                    _Rec => _Rec.SenderPhoneNumber == this.g_AppUser.Phone
                ).OrderByDescending( _Rec => _Rec.DateAndTime ).FirstOrDefault(); //Maybe filter to messages received in the last 1 hour or so?
            }
        }

        public MessagingResponse handle( out string _sMessageSent /*for simulation*/) {
            _sMessageSent = string.Empty;
            try {
                bool _bIsTripCode = this.determineTrip();
                bool _bIsParcelCode = this.determineParcel();
                this.g_iAppUserTypeID = this.determineAppUserType();
                if ( _bIsTripCode && this.g_iAppUserTypeID == ( int )EnumAppUserType.business_admin ) {
                    //Just send location of transporter or status of trip if not enroute
                    this.g_iResponseProcessID = ( int )EnumAppProcessMain.home_or_none;
                    this.g_iResponseSubProcessID = ( int )EnumWelcomeSub.greeting_and_menu;
                    return this.sendTripLocationDetail( out _sMessageSent );
                }
                if ( _bIsParcelCode && new List<int> { ( int )EnumAppUserType.parcel_sender, ( int )EnumAppUserType.parcel_receiver }.Contains( this.g_iAppUserTypeID ) ) {
                    //Just send location of transporter or status of parcel if not enroute
                    this.sendTripLocationDetail( out _sMessageSent );
                }
                if ( this.isHello() ) {
                    this.g_iResponseProcessID = ( int )EnumAppProcessMain.welcome_message;
                    this.g_iResponseSubProcessID = ( int )EnumWelcomeSub.greeting_and_menu;
                    return sayHello( out _sMessageSent );
                }
                if ( ( this.g_PreviousMessage == null || this.g_PreviousMessage.MessageID == 0 ) && this.g_iAppUserTypeID == ( int )EnumAppUserType.guest ) {
                    this.g_iResponseProcessID = ( int )EnumAppProcessMain.welcome_message;
                    this.g_iResponseSubProcessID = ( int )EnumWelcomeSub.greeting_and_menu;
                    return sayHello( out _sMessageSent );
                }
                //this.determineResponseMessage();
                return new MessagingResponse();
            } catch { }
            return new MessagingResponse();
        }

        private MessagingResponse sayHello( out string _sMessageSent ) {
            _sMessageSent = string.Empty;
            try {
                this.deterResponseMessageTemplate();
                return this.sendResponse( out _sMessageSent );
            } catch { }
            return null;
        }

        private MessagingResponse sendTripLocationDetail( out string _sMessageSent ) {
            _sMessageSent = string.Empty;
            try {
                this.deterResponseMessageTemplate();
                this.buildResponseMessage( g_ResponseMessageTemplate );
                return this.sendResponse( out _sMessageSent );
            } catch { }
            return null;
        }

        private bool deterResponseMessageTemplate() {
            try {
                if ( g_iResponseProcessID == 0 || g_iResponseSubProcessID == 0 ) {
                    return false; //We call this method only after setting those 2 variables
                }
                ProcessTemplate _ProcessTemplate = g_UnitOfWork.ProcessTemplates.getFiltered( _Rec => _Rec.ProcessID == g_iResponseProcessID && _Rec.SubProcessID == g_iResponseSubProcessID ).FirstOrDefault();
                if ( _ProcessTemplate == null ) {
                    return false; //Maybe reset g_iCurrentProcessID and g_iCurrentSubProcessID to home page? And call this method if it wasn't the one called
                }
                this.g_bMessageReceivedValid = this.validateMessageReceived();
                IEnumerable<ResponseMessageTemplate> _ResponseMessageTemplates = g_UnitOfWork.ResponseMessageTemplates.getFiltered(
                    _Rec => _Rec.MessageID == ( this.g_bMessageReceivedValid ? _ProcessTemplate.SuccessMessageID : _ProcessTemplate.FailedMessageID )
                );
                if ( _ResponseMessageTemplates == null || _ResponseMessageTemplates.Count() == 0 ) {
                    return false; //Maybe reset g_iCurrentProcessID and g_iCurrentSubProcessID to home page? And call this method if it wasn't the one called
                }
                ResponseMessageTemplate _ResponseMessageTemplate = null;
                if ( !_ProcessTemplate.OnSuccessMessageOthers ) {
                    g_OnSuccessOthersResponseMessageTemplates = null;
                    _ResponseMessageTemplate = _ResponseMessageTemplates.Count() > 1 ?
                                                                        _ResponseMessageTemplates.Where( _Rec => _Rec.AppUserTypeID == this.g_iAppUserTypeID ).FirstOrDefault() :
                                                                        _ResponseMessageTemplates.FirstOrDefault();
                } else {
                    g_OnSuccessOthersResponseMessageTemplates = _ResponseMessageTemplates.ToList().Where( _Rec => _Rec.AppUserTypeID != ( int )EnumAppUserType.message_sender );
                    _ResponseMessageTemplate = _ResponseMessageTemplates.ToList().Where( _Rec => _Rec.AppUserTypeID == ( int )EnumAppUserType.message_sender ).FirstOrDefault();
                }
                g_ResponseMessageTemplate = _ResponseMessageTemplate;
                return g_ResponseMessageTemplate != null;
            } catch { }
            return false;
        }

        private string replacePlaceHolders( string _sText ) {
            if ( _sText.Contains( "{Nickname}" ) ) {
                _sText = _sText.Replace( "{Nickname}", this.g_AppUser.Nickname );
            }
            if ( _sText.Contains( "{TripCode}" ) ) {
                _sText = _sText.Replace( "{TripCode}", this.g_Trip.TripCode );
            }
            if ( _sText.Contains( "{ParcelCode}" ) ) {
                _sText = _sText.Replace( "{ParcelCode}", this.g_Parcel.ParcelCode );
            }
            return _sText;
        }

        private string buildResponseMessage( ResponseMessageTemplate _ResponseMessageTemplate ) {
            try {
                if ( g_ResponseMessageTemplate == null || g_ResponseMessageTemplate.ResponseMessageTemplateID == 0 ) {
                    return string.Empty;
                }
                string _sMessage = string.Empty;
                if ( !string.IsNullOrEmpty( g_ResponseMessageTemplate.PrefixText ) ) {
                    _sMessage += $"{this.replacePlaceHolders( g_ResponseMessageTemplate.PrefixText )}{ Environment.NewLine }";
                }
                if ( g_ResponseMessageTemplate.HasStaticOptions ) {
                    IEnumerable<StaticMessageOption> _StaticMessageOptions = g_UnitOfWork.StaticMessageOptions.getFiltered( _Rec => _Rec.MessageTemplateID == g_ResponseMessageTemplate.ResponseMessageTemplateID );
                    if ( _StaticMessageOptions != null && _StaticMessageOptions.Count() > 0 ) {
                        foreach ( StaticMessageOption _StaticMessageOption in _StaticMessageOptions ) {
                            _sMessage += $"{_StaticMessageOption.OptionValueID }. {_StaticMessageOption.Text}{ Environment.NewLine }";
                        }
                    }
                } else if ( g_ResponseMessageTemplate.HasDynamicOptions ) {
                    List<DynamicMessageOption> _DynamicMessageOptions = getDynamicMessageOptions();
                    if ( _DynamicMessageOptions != null && _DynamicMessageOptions.Count() > 0 ) {
                        foreach ( DynamicMessageOption _DynamicMessageOption in _DynamicMessageOptions ) {
                            g_UnitOfWork.DynamicMessageOptions.insertOrUpdate( _DynamicMessageOption );
                            if ( g_UnitOfWork.commit( out string _sError ) ) {
                                _sMessage += $"{_DynamicMessageOption.OptionValueID }. {_DynamicMessageOption.Text}{ Environment.NewLine }";
                            }
                        }
                    }
                }
                if ( !string.IsNullOrEmpty( g_ResponseMessageTemplate.SuffixText ) ) {
                    _sMessage += g_ResponseMessageTemplate.SuffixText;
                }
                if ( g_ResponseMessageTemplate.HasBackOption && !string.IsNullOrEmpty( g_ResponseMessageTemplate.BackOptionText ) ) {
                    _sMessage += $"{ Environment.NewLine }{g_ResponseMessageTemplate.BackOptionText}";
                }
                return _sMessage;
            } catch { }
            return string.Empty;
        }

        private bool validateMessageReceived() {
            try {
                switch ( ( EnumAppProcessMain )g_iResponseProcessID ) {
                    case EnumAppProcessMain.welcome_message:
                        switch ( ( EnumWelcomeSub )g_iResponseSubProcessID ) {
                            case EnumWelcomeSub.greeting_and_menu:
                                return true;
                        }
                        break;
                }
                return true;
            } catch { }
            return false;
        }

        private bool isHello() {
            try {
                return new List<string> {
                    "Hi",
                    "Hello",
                    "Hey",
                    "Hie",
                    "##"
                }.Contains( g_IncomingMessage.Body );
            } catch { }
            return false;
        }

        private bool determineTrip() {
            try {
                if ( g_IncomingMessage.Body.Length != Constant.TRIP_CODE_LENGTH ) {
                    return false;
                }
                Trip _Trip = g_UnitOfWork.Trips.getFiltered( _Rec =>
                        _Rec.TripCode == g_IncomingMessage.Body &&
                        _Rec.TripStatusID != ( int )EnumTripStatus.archived//Maybe we auto-archive trips one day after "arrived"?
                ).FirstOrDefault();
                if ( _Trip != null ) {
                    this.g_Trip = _Trip;
                    return true;
                }
            } catch { }
            return false;
        }

        private bool determineParcel() {
            try {
                if ( g_IncomingMessage.Body.Length != Constant.TRIP_CODE_LENGTH ) {
                    return false;
                }
                Parcel _Parcel = g_UnitOfWork.Parcels.getFiltered( _Rec =>
                        _Rec.ParcelCode == g_IncomingMessage.Body &&
                        _Rec.Trip.TripStatusID == ( int )EnumTripStatus.archived //Maybe we auto-archive trips one day after "arrived"?
                ).FirstOrDefault();
                if ( _Parcel != null ) {
                    this.g_Parcel = _Parcel;
                    return true;
                }
            } catch { }
            return false;
        }

        private int determineAppUserType() {
            try {
                if ( g_PreviousMessage != null && g_PreviousMessage.MessageID != 0 ) {
                    return g_PreviousMessage.AppUserTypeID; //Assume the type in the previous message
                }
                if ( AppUser.SUPER_ADMINS.Where( _Rec => _Rec.Phone == this.g_sAppUserNumber ).FirstOrDefault() != null ) {
                    return ( int )EnumAppUserType.super_admin;
                }
                //If parcel is set, it's either sender of receiver
                if ( g_Parcel != null && g_Parcel.ParcelID != Guid.Empty ) {
                    if ( g_Parcel.SenderAppUserID == this.g_AppUser.AppUserID ) {
                        return ( int )EnumAppUserType.parcel_sender;
                    }
                    if ( g_Parcel.RecipientAppUserID == this.g_AppUser.AppUserID ) {
                        return ( int )EnumAppUserType.parcel_receiver;
                    }
                }
                //If trip is set, it's either driver or admin
                if ( g_Trip != null && g_Trip.TripID != Guid.Empty ) {
                    if ( g_Trip.AppUserDriver.AppUserID == this.g_AppUser.AppUserID ) {
                        return ( int )EnumAppUserType.transporter_or_driver;
                    }
                    if ( g_UnitOfWork.BusinessAdmins.getFiltered( _Rec =>
                        _Rec.AppUserID == this.g_AppUser.AppUserID &&
                        _Rec.BusinessAdminStatusID == ( int )EnumBusinessAdminStatus.active
                        ).FirstOrDefault() != null ) {
                        return ( int )EnumAppUserType.business_admin;
                    }
                }
            } catch { }
            return ( int )EnumAppUserType.guest;
        }

        private MessagingResponse sendResponse( out string _sMessageSent ) {
            MessagingResponse _MessagingResponse = new MessagingResponse();
            _sMessageSent = string.Empty;
            try {
                _sMessageSent = this.buildResponseMessage( this.g_ResponseMessageTemplate );
                if ( !g_bSimulating ) {
                    _MessagingResponse.Message( _sMessageSent );
                }
                this.logMessage( _sMessageSent, this.g_ResponseMessageTemplate.ResponseMessageTemplateID, this.g_sAppUserNumber, _bIsOtherUser: false, this.g_iAppUserTypeID );
                List<MessageRecipient> _OtherMessageRecipients = this.getOtherRecipients();
                MessageRecipient _MessageRecipient = null;
                if ( this.g_OnSuccessOthersResponseMessageTemplates != null && g_OnSuccessOthersResponseMessageTemplates.Count() > 0 ) {
                    foreach ( ResponseMessageTemplate _ResponseMessageTemplate in this.g_OnSuccessOthersResponseMessageTemplates ) {
                        _sMessageSent = this.buildResponseMessage( _ResponseMessageTemplate );
                        _MessageRecipient = _OtherMessageRecipients.Where( _Rec => _Rec.UserTypeID == _ResponseMessageTemplate.AppUserTypeID ).FirstOrDefault();
                        this.sendFromScratch( _sMessageSent, _ResponseMessageTemplate.ResponseMessageTemplateID, _MessageRecipient.PhoneNumber, _MessageRecipient.UserTypeID );
                        return null;
                    }
                }
            } catch { }
            return _MessagingResponse;
        }

        private List<MessageRecipient> getOtherRecipients() {
            try {
                return new List<MessageRecipient>(); //build from g_OnSuccessOthersResponseMessageTemplates 
            } catch { }
            return null;
        }

        private List<DynamicMessageOption> getDynamicMessageOptions() {
            try {
                return new List<DynamicMessageOption>(); //build from g_OnSuccessOthersResponseMessageTemplates 
            } catch { }
            return null;
        }

        private bool sendFromScratch( string _sMessageSent, int _iMessageTemplateID, string _sToPhoneNumber, int _iUserTypeID ) {
            try {
                if ( g_bSimulating ) {
                    return logMessage( _sMessageSent, _iMessageTemplateID, _sToPhoneNumber, _bIsOtherUser: true, _iUserTypeID );
                }
                TwilioClient.Init( Constant.TWILIO_ACCOUNT_SID, Constant.TWILIO_AUTH_TOKEN );
                CreateMessageOptions _CreateMessageOption = new CreateMessageOptions( new PhoneNumber( $"whatsapp:{_sToPhoneNumber}" ) );
                _CreateMessageOption.From = new PhoneNumber( $"whatsapp:{Constant.TWILIO_OUR_NUMBER}" );
                _CreateMessageOption.Body = _sMessageSent;
                MessageResource _MessageResource = MessageResource.Create( _CreateMessageOption );
                if ( !double.TryParse( _MessageResource.Price, out double _dPriceBilled ) ) {
                    _dPriceBilled = 0;
                }
                g_dMessageSentCost = _dPriceBilled;
                return !string.IsNullOrEmpty( _MessageResource.Sid ) && logMessage( _sMessageSent, _iMessageTemplateID, _sToPhoneNumber, _bIsOtherUser: true, _iUserTypeID );
            } catch { }
            return false;
        }

        private bool logMessage( string _sMessageSent, int _iMessageTemplateID, string _sToPhoneNumber, bool _bIsOtherUser, int _iUserTypeID ) {
            try {
                g_UnitOfWork.ConversationLogs.insertOrUpdate( new Message {
                    AppUserTypeID = _iUserTypeID,
                    IsTemplateResponse = _bIsOtherUser, //Template responses are for other users and should not be considered when checking conversation flow
                    Cost = g_dMessageSentCost,
                    BodyReceived = g_sMessageReceived,
                    BodySent = _sMessageSent,
                    DateAndTime = DateTime.Now,
                    IsValid = g_bMessageReceivedValid,
                    TwilioMessageID = g_IncomingMessage.SmsSid,
                    ParcelID = g_Parcel?.ParcelID,
                    SenderPhoneNumber = _sToPhoneNumber,
                    TripID = g_Trip?.TripID,
                    ResponseMessageTemplateID = _iMessageTemplateID,
                    ProcessID = this.g_iResponseProcessID,
                    SubProcessID = this.g_iResponseSubProcessID
                } );
                return true;
            } catch { }
            return false;
        }
    }
}