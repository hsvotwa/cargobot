﻿public enum EnumBusinessStatus
{
    none = 0,
    active = 1,
    suspended = 2,
    unpaid = 3
}

public enum EnumBusinessAdminStatus
{
    active = 0,
    deleted = 1,
    invited_to_chat = 2
}

public enum EnumTripStatus
{
    loading = 1,
    departed = 2,
    arrived = 3,
    archived = 4
}

public enum EnumParcelStatus
{
    enroute = 1,
    pending_collection = 2,
    collected = 3
}

public enum EnumAppUserType
{
    message_sender = 0,
    guest = 1,
    business_admin = 2,
    parcel_sender = 3,
    parcel_receiver = 4,
    transporter_or_driver = 5,
    super_admin = 100
}

public enum EnumMessageMedium
{
    call = 1,
    sms = 2,
    whatsapp = 3,
    email = 4
}

public enum EnumResponseStatus
{
    success = 1,
    failed = 2,
    error = 3,
    warning = 4, //
    info = 5, //For stuff like "Already saved"
}

public enum EnumApiResponseStatus
{
    success = 1,
    error = 2,
    not_found = 3,
    unauthorised = 4,
    invalid_data = 5,
    empty_resultset = 6
}

public enum EnumYesNo
{
    yes = 1,
    no = 2,
    none = 3,
}

public enum EnumDynamicListType
{
    drivers = 1,
    vehicles = 2,
    parcel_receivers = 3,
    route_locations = 4,
    trips = 5,
    sender_parcels = 6,
    receiver_parcels = 7
}

public enum EnumMessageType
{
    welcome = 1,
    question = 2,
    invalid_input = 3,
    error = 4,
    success = 5
}

public enum EnumResponseMessageTemplateStatus
{
    active = 1,
    inactive = 2,
    replaced = 3
}