﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Text.RegularExpressions;

public class Conv {
    public static string toNum( string _sValue, bool _bToSql = false, int _iDec = 2, string _sZeroDesc = "0.00", bool _bAsCurrency = false ) {
        try {
            _sValue = _sValue.Trim().Replace( ",", "" ).Replace( " ", "" );
            if ( !_bToSql ) {
                if ( _sValue != string.Empty ) {
                    NumberFormatInfo _NumFrmInfo = new NumberFormatInfo();
                    _NumFrmInfo.NumberDecimalDigits = _iDec;
                    _NumFrmInfo.NumberDecimalSeparator = ".";
                    _NumFrmInfo.NumberGroupSeparator = ",";
                    _sValue = (
                            double.Parse( _sValue ) != 0
                            ? double.Parse( _sValue.Trim() ).ToString( "N", _NumFrmInfo )
                            : _sZeroDesc
                        );
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Convert", "toNum", _Ex.Message );
        }
        return ( !_bAsCurrency ? "" : Constant.CURRENCY_SYMBOL ) + _sValue;
    }

    public static string toCamelCase( string _sValue ) {
        string _sReturn = "";
        string[] _sWords = _sValue.Trim().ToLower().Replace( "  ", " " ).Split( ' ' );
        try {
            foreach ( string _sWord in _sWords ) {
                if ( _sWord.Trim() != string.Empty ) {
                    if ( _sReturn.Trim() != string.Empty ) { _sReturn += " "; }
                    _sReturn += _sWord.Substring( 0, 1 ).ToUpper() + _sWord.Substring( 1, _sWord.Length - 1 );
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Convert", "toNum", _Ex.Message );
        }
        return _sReturn;
    }

    public static string toDate( string _sValue, bool _bToSql = false, bool _bDateOnly = false, bool _bTimeOnly = false, bool _bLongTime = false, string _sFormat = "" ) {
        string _sDateFrmt = "", _sTimeFrmt = "";
        DateTime _dtDateTime = DateTime.Now;
        try {
            if ( DateTime.TryParse( _sValue, out _dtDateTime ) ) {
                _sDateFrmt = (
                        !_bToSql
                        ? "dd/MM/yyyy" //convert to the format set in configurations
                        : Constant.SQL_DATE_FORMAT_SHORT
                    );
                if ( !_bDateOnly ) {
                    _sTimeFrmt = " ";
                    if ( !_bToSql ) {
                        _sTimeFrmt += (
                                !_bLongTime
                                ? "HH:mm"
                                : Constant.TIME_FORMAT_LONG
                            );
                    } else {
                        _sTimeFrmt += Constant.TIME_FORMAT_LONG;
                    }
                }
                _sValue = _dtDateTime.ToString(
                        _sDateFrmt + _sTimeFrmt,
                        DateTimeFormatInfo.InvariantInfo
                    );
                if ( _sFormat != "" ) {
                    return _dtDateTime.ToString(
                         _sFormat,
                         DateTimeFormatInfo.InvariantInfo
                     );
                }
                if ( _bTimeOnly ) {
                    _sValue = _dtDateTime.ToString( _sTimeFrmt.Trim(),
                      DateTimeFormatInfo.InvariantInfo
                  );
                } else if ( _bDateOnly ) {
                    _sValue = _dtDateTime.ToString( _sDateFrmt,
                      DateTimeFormatInfo.InvariantInfo
                  );
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Convert", "toDate", _Ex.Message );
        }
        if ( _sValue.Length < 10 ) {
            _sValue = ( _sValue.StartsWith( "0" ) ) ? _sValue.Insert( 3, "0" ) : _sValue.Insert( 0, "0" );
        }
        return _sValue;
    }

    public static string toUpperFirst( string _sValue ) {
        try {
            return char.ToUpper( _sValue[ 0 ] ) + _sValue.Substring( 1 );
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Convert", "toUpperFirst", _Ex.Message );
        }
        return _sValue;
    }
}