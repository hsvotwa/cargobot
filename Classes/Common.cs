﻿using System;
using System.Collections.Generic;
public static class Common {
    public static string GetJobRefNo() {
        return DateTime.Now.Hour.ToString().Trim() + DateTime.Now.Minute.ToString().Trim() + DateTime.Now.Second.ToString().Trim() + DateTime.Now.Millisecond.ToString().Trim() + DateTime.Today.Day.ToString().Trim() + DateTime.Today.Month.ToString().Trim() + DateTime.Today.Year.ToString();
    }

    public static string getRecordHeaderText(string _sWholeText, int _iLettersToShow = 50) {
        try {
            _iLettersToShow = (_iLettersToShow < 10 ? 10 : _iLettersToShow); //Set minimum letters to show to 10
            if (_sWholeText.Length > _iLettersToShow) {
                _sWholeText = _sWholeText.Substring(0, _iLettersToShow - 3) + "..."; // if letters to show is less than string length, show part of string plus ellipsis (...)
            }
            return " - " + _sWholeText; //prepend a hiphen
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "getRecordHeaderText", _Ex.Message);
        }
        return _sWholeText;
    }

    public static string getEllipsisText(string _sWholeText, int _iLettersToShow = 30) {
        try {
            _iLettersToShow = (_iLettersToShow < 10 ? 10 : _iLettersToShow); //Set minimum letters to show to 10
            if (_sWholeText.Length > _iLettersToShow) {
                _sWholeText = _sWholeText.Substring(0, _iLettersToShow - 3) + "..."; // if letters to show is less than string length, show part of string plus ellipsis (...)
            }
            return _sWholeText; //prepend a hiphen
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "getEllipsisText", _Ex.Message);
        }
        return _sWholeText;
    }

    public static bool validImageFileExtensions(string _sFileExt) {
        string[] _sAllowableExtensions = { ".jpg", ".png", ".jpeg" };
        try {
            foreach (string _sVal in _sAllowableExtensions) {
                if (_sVal.Contains(_sFileExt.ToLower())) {
                    return true;
                }
            }
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "validImageFileExtensions", _Ex.Message);
        }
        return false;
    }

    public static bool validFileExtensions(string _sFileExt) {
        string[] _sAllowableExtensions = { ".pdf", ".docx", ".xlsx" };
        try {
            foreach (string _sVal in _sAllowableExtensions) {
                if (_sVal.Contains(_sFileExt.ToLower())) {
                    return true;
                }
            }
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "validFileExtensions", _Ex.Message);
        }
        return false;
    }

    public static string getDescriptiveTime(DateTime _dtDate) {
        try {
            DateTime _dtNow = DateTime.Now;
            bool _bPast = _dtDate < _dtNow;
            TimeSpan _TimeSpan = _dtNow.Subtract(_dtDate);
            int _iDayDiff = (int)_TimeSpan.TotalDays;
            int _iHourDiff = (int)_TimeSpan.TotalHours;
            int _iMinDiff = (int)_TimeSpan.TotalMinutes;
            int _iSecDiff = (int)_TimeSpan.TotalSeconds;
            _iDayDiff = (_iDayDiff < 0 ? _iDayDiff * -1 : _iDayDiff);
            _iHourDiff = (_iHourDiff < 0 ? _iHourDiff * -1 : _iHourDiff);
            _iMinDiff = (_iMinDiff < 0 ? _iMinDiff * -1 : _iMinDiff);
            _iSecDiff = (_iSecDiff < 0 ? _iSecDiff * -1 : _iSecDiff);
            if (_iDayDiff == 0) {
                if (_iSecDiff < 60) {
                    return "Just now";
                }
                int _iHoursToday = _iMinDiff / 60;
                int _iMins = _iMinDiff % 60;
                return getDescriptiveTimeTense((_iHoursToday == 0 ? "" : _iHoursToday.ToString() + " hour" + (_iHoursToday == 1 ? "" : "s")) +
                                                                        (_iMins != 0 && _iHoursToday != 0 ? ", " : "") +
                                                                        (_iMins == 0 ? "" : _iMins.ToString() + " minute" + (_iMins == 1 ? "" : "s")),
                                                                        _bPast);
            }
            if (_iDayDiff < 7) {
                int _iDaysHr = _iHourDiff / 24;
                int _iHours = _iHourDiff % 24;
                return getDescriptiveTimeTense(_iDaysHr.ToString() + " day" +
                                                                    (_iDaysHr == 1 ? "" : "s") +
                                                                    (_iHours == 0 ? "" : ", " + _iHours.ToString() + " hour" + (_iHours == 1 ? "" : "s")),
                                                                    _bPast);
            }
            if (_iDayDiff < 31) {
                int _iWeeks = _iDayDiff / 7;
                int _iDaysWeek = _iDayDiff % 7;
                return getDescriptiveTimeTense(_iWeeks.ToString() + " week" +
                                                                    (_iWeeks == 1 ? "" : "s") +
                                                                    (_iDaysWeek == 0 ? "" : ", " + _iDaysWeek.ToString() + " day" + (_iDaysWeek == 1 ? "" : "s")),
                                                                    _bPast);
            }
            if (_iDayDiff < 365) {
                int _iMonth = _iDayDiff / 31;
                int _iDaysMonth = _iDayDiff % 31;
                return getDescriptiveTimeTense(_iMonth.ToString() + " month" +
                                                                    (_iMonth == 1 ? "" : "s") +
                                                                    (_iDaysMonth == 0 ? "" : ", " + _iDaysMonth.ToString() + " day" + (_iDaysMonth == 1 ? "" : "s")),
                                                                    _bPast);
            }
            int _iYear = _iDayDiff / 365;
            return getDescriptiveTimeTense(_iYear.ToString() + " year" +
                                                                (_iYear == 1 ? "" : "s"),
                                                                _bPast) + ", on " + _dtDate.ToString("dd MMM HH:mm");
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "getDescriptiveTime", _Ex.Message);
        }
        return "On " + _dtDate.ToString();
    }

    private static string getDescriptiveTimeTense(string _sTimeDescr, bool _bPast) {
        if (_bPast) {
            return _sTimeDescr + " ago";
        }
        return _sTimeDescr + " left";
    }

    public static string getRandomText(int _iLength) {
        try {
            Random random = new Random(_iLength);
            string _sChar = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
            string _sReturn = string.Empty;
            Random _Rm = new Random();
            for (int i = 0; i < _iLength; i++) {
                _sReturn += _sChar[_Rm.Next(0, _sChar.Length - 1)];
            }
            return _sReturn;
        } catch (Exception _Ex) {
            ErrorHandler.logError("Class", "Common", "getRandomText", _Ex.Message);
        }
        return "1T5AW350M3";
    }

    public static string getStatisticsChangeDescription(int _iPrevious, int _iCurrent, string _sPeriodDescription = "month") {
        try {
            string _sRet = "";
            if (_iPrevious == _iCurrent) {
                return _sRet = $"Same as this time last {_sPeriodDescription}";
            }
            if (_iPrevious == 0) {
                return _sRet = $"No records available for last {_sPeriodDescription}";
            }
            int _iDifference = 100 * (_iPrevious - _iCurrent) / _iPrevious;
            if (_iDifference == 0) { //Not same number but percentage is rounded to zero
                return _sRet = $"Insignificant change from this time last {_sPeriodDescription}";
            }
            if (_iPrevious < _iCurrent) {
                _iDifference *= -1;
                _sRet = $"Increased by { _iDifference }% from this time last {_sPeriodDescription}";
            }
            if (_iPrevious > _iCurrent) {
                _sRet = $"Decreased by { _iDifference }% from this time last {_sPeriodDescription}";
            }
            return _sRet;
        } catch {
        }
        return $"Last {_sPeriodDescription} value was {_iPrevious}";
    }
}