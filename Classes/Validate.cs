﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

public class Validate {
    public static bool isEmail( string _sValue ) {
        try {
            _sValue = _sValue.Trim();
            if ( _sValue == String.Empty ) { return false; }
            _sValue = Regex.Replace( _sValue, @"(@)(.+)$", getDomainMapper );  //Use IdnMapping class to convert Unicode domain names.
            return Regex.IsMatch(
                    _sValue,
                    @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z]{2,17}))$",
                    RegexOptions.IgnoreCase
                );
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "isEmail", _Ex.Message );
        }
        return false;
    }

    public static bool isNamibianPhone( string _sValue ) {
        try {
            _sValue = _sValue.Trim();

            int _iOut = 0;

            if ( _sValue.Length != 10 || !int.TryParse( _sValue.Substring( 1 ), out _iOut ) || ( !_sValue.StartsWith( "081" ) && !_sValue.StartsWith( "085" ) ) ) {
                return false;
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "isNamibianPhone", _Ex.Message );
        }
        return true;
    }

    private static string getDomainMapper( Match _mMatch ) {
        IdnMapping _Idn = new IdnMapping();  //IdnMapping class with default property values.

        try {
            string _sDomainName = _mMatch.Groups[ 2 ].Value;
            _sDomainName = _Idn.GetAscii( _sDomainName );
            return _mMatch.Groups[ 1 ].Value + _sDomainName;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "getDomainMapper", _Ex.Message );
        }
        return "";
    }

    public static bool isGuid( string _sValue ) {
        try {
            _sValue = _sValue.Trim();
            if ( _sValue.Length != 36 ) { return false; }
            //Regex _RegexGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);
            Regex _Regex = new Regex( @"^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$", RegexOptions.Compiled );
            return _Regex.IsMatch( _sValue );
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "isGuid", _Ex.Message );
        }
        return false;
    }

    public static bool isDate( string _sValue, bool _bSqlFmt = false ) {
        try {
            _sValue = _sValue.Trim();
            if ( _sValue.Length != 10 ) { return false; }
            Regex _Regex = new Regex(
                    (
                        !_bSqlFmt
                        ? @"^[0-9]{2}/[0-9]{2}/[0-9]{4}$"
                        : @"^[0-9]{4}-[0-9]{2}-[0-9]{2}$"
                    ),
                    RegexOptions.Compiled
                );
            return _Regex.IsMatch( _sValue );
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "isDate", _Ex.Message );
        }

        return false;
    }

    public static bool hasAccepDateSep( string _sValue, out string _sSeparator ) {
        _sSeparator = string.Empty;

        try {
            string[] _sSeparators = new string[] { "/", "-", "." };

            foreach ( string _sSep in _sSeparators ) {
                if ( _sValue.Contains( _sSep ) ) {
                    _sSeparator = _sSep;
                    break;
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "hasAccepDateSep", _Ex.Message );
        }

        return _sSeparator.Trim() != String.Empty;
    }

    public static bool isBetween( int _iValue, int _iMin, int _iMax, bool _bIncl = true ) {
        try {
            return (
                _bIncl
                ? _iValue >= _iMin && _iValue <= _iMax
                : _iValue > _iMin && _iValue < _iMax
            );
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Valid", "isBetween", _Ex.Message );
        }
        return false;
    }

    public static bool isCellNo( string _sVal ) {
        try {
            if ( !double.TryParse( _sVal.Trim().Replace( " ", "" ), out double _dParseTo ) ) {
                return false;
            }
            return _sVal.Length == 12;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Valid", "isPhone", _Ex.Message );
        }
        return false;
    }

    public static bool isTelNo( string _sVal ) {
        try {
            if ( !double.TryParse( _sVal.Trim().Replace( " ", "" ), out double _dParseTo ) ) {
                return false;
            }
            return _sVal.Length <= 12;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Valid", "isTelNo", _Ex.Message );
        }
        return false;
    }
}