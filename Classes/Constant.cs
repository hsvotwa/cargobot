﻿public static class Constant {
    public const string SUPPORT_NUMBER = "+264 81 337 2739";
    public const string APP_SHORT_NAME = "CargoBot";
    public const string APP_NAME = "CargoBot";
    public const string DB_PASSWORD = "geekS@#5214";
    //public const string DB_USERNAME = "geekabyt_dieter";
    //Start Company Detail
    public const string COMPANY_NAME = "Geratin && CS Software Engineers";
    public const string WEBSITE_URL = "cargobot.cssoftwareengineers.co.za";
    public const string COMPANY_PHONE = "+264 81 337 2739";
    public const string COMPANY_EMAIL = "cargobot@cssoftwareengineers.co.za";

    public const string TWILIO_ACCOUNT_SID = "ACa7dca9790457aea4ecfcd5e9dcd0d67f";
    public const string TWILIO_AUTH_TOKEN = "3aedc465ef348a67128c32729efc9dbf";
    public const string TWILIO_OUR_NUMBER = "+14155238886";
    //End Company Detail
    //public const string DB_NAME = "geekabyt_covid";

    //Settings
    public const int TRIP_CODE_LENGTH = 10;
    public const int PARCEL_CODE_LENGTH = 14;

#if DEBUG
    public const string DB_NAME = "CargoBot";
    public const string DB_USERNAME = "sa";
#else
    public const string DB_NAME = "cssoftwa_cargo_bot";
    public const string DB_USERNAME = "cssoftwa_geratin";

#endif

    public const string CON_STRING = "Server=.; Database=" + DB_NAME + "; User ID=" + DB_USERNAME + "; Password=" + DB_PASSWORD + "; MultipleActiveResultSets=true";
    public const string DATE_FORMAT_SHORT = "dd/MM/yyyy";
    public const string DATE_TIME_FORMAT_SHORT = "dd/MM/yyyy HH:mm";
    public const string DATE_TIME_FORMAT_LONG = "dd MMMM yyyy HH:mm";
    public const string TIME_FORMAT_LONG = "HH:mm:ss";
    public const string TIME_FORMAT_SHORT = "HH:mm";
    public const string SQL_DATE_FORMAT_SHORT = "yyyy-MM-dd";
    public const string SQL_DATE_FORMAT_LONG = "yyyy-MM-dd HH:mm:ss";
    public const string SRVR_DATE_FORMAT_SHORT = "m/d/yyyy";
    public const string NUMBER_FORMAT = "##,#0.00";
    //SMTP
    public const string SMTP_USERNAME = "info@geratin.net";
    public const string SMTP_PASSWORD = "bwwvdvvmvmbjnspy";
    public const string SMTP_SERVER = "smtp.gmail.com";
    public const int PORT_NUMBER = 587;
    public const bool ENABLE_SSL = true;
    public const string FROM_EMAIL = "\"" + APP_SHORT_NAME + "\" <info@geratin.net>";

    //End SMTP
    public const string CURRENCY_SYMBOL = "$";
    public const string FMT_MYSQL_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    public const string FMT_MYSQL_DATE = "yyyy-MM-dd";
    public const string FMT_MYSQL_ANNIV_DAY = "MM-dd";
    public const string FMT_DATE_TIME = "dd MMM yyyy HH:mm:ss";
    public const string SIGNATUTE_IMAGE_URL = @"https://gtcheckin.geratin.net/assets/images/logo_gt.png";
    public const string DOMAIN_URL = "cargobot.cssoftwareengineers.co.za";
    public const string EMAIL_SALUTATION = @"Cheers,";
    public const string RESOURCES_FOLDER_NAME = "Resources/";
}

public static class SqlTable {
    public const string LU_BUSINESS_STATUS = "tbl_lu_business_status";
    public const string LU_APP_USER_TYPE = "tbl_lu_app_user_type";
    public const string LU_TRIP_STATUS = "tbl_lu_trip_status";
    public const string LU_PARCEL_STATUS = "tbl_lu_parcel_status";
    public const string LU_BUSINESS_ADMIN_STATUS = "tbl_lu_business_admin_status";
    public const string LU_DYNAMIC_LIST_TYPE = "tbl_lu_dynamic_list_type";
    public const string LU_MESSAGE_TYPE = "tbl_lu_message_type";
    public const string LU_RESPONSE_MESSAGE_TEMPLATE_STATUS = "tbl_lu_response_message_template_status";
    public const string TBL_BUSINESS = "tbl_business";
    public const string TBL_BUSINESS_ADMIN = "tbl_business_admin";
    public const string TBL_APP_USER = "tbl_app_user";
    public const string TBL_VEHICLE = "tbl_vehicle";
    public const string TBL_TRANSPORTER = "tbl_transporter";
    public const string TBL_TRIP = "tbl_trip";
    public const string TBL_VEHICLE_CHECKIN = "tbl_vehicle_checkin";
    public const string TBL_MESSAGE = "tbl_message";
    public const string TBL_PARCEL = "tbl_parcel";
    public const string TBL_PARCEL_IMAGE = "tbl_parcel_image";
    public const string TBL_CONVERSATION_LOG = "tbl_conversation_log";
    public const string TBL_PROCESS = "tbl_process";
    public const string TBL_PROCESS_TEMPLATE = "tbl_process_template";
    public const string TBL_RESPONSE_MESSAGE_TEMPLATE = "tbl_response_message_template";
    public const string TBL_PROCESS_SEQUENCE = "tbl_process_sequence";
    public const string TBL_STATIC_MESSAGE_OPTION = "tbl_static_message_option";
    public const string TBL_DYNAMIC_MESSAGE_OPTION = "tbl_dynamic_message_option";
}