﻿namespace CargoBot.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Templatesdata : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tbl_transporter",
                c => new
                    {
                        TransporterID = c.Guid(nullable: false),
                        BusinessAdminStatusID = c.Int(nullable: false),
                        AppUserID = c.Guid(nullable: false),
                        BusinessID = c.Guid(nullable: false),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.TransporterID)
                .ForeignKey("dbo.tbl_app_user", t => t.AppUserID)
                .ForeignKey("dbo.tbl_business", t => t.BusinessID)
                .ForeignKey("dbo.tbl_lu_business_admin_status", t => t.BusinessAdminStatusID)
                .Index(t => t.BusinessAdminStatusID)
                .Index(t => t.AppUserID)
                .Index(t => t.BusinessID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.tbl_transporter", "BusinessAdminStatusID", "dbo.tbl_lu_business_admin_status");
            DropForeignKey("dbo.tbl_transporter", "BusinessID", "dbo.tbl_business");
            DropForeignKey("dbo.tbl_transporter", "AppUserID", "dbo.tbl_app_user");
            DropIndex("dbo.tbl_transporter", new[] { "BusinessID" });
            DropIndex("dbo.tbl_transporter", new[] { "AppUserID" });
            DropIndex("dbo.tbl_transporter", new[] { "BusinessAdminStatusID" });
            DropTable("dbo.tbl_transporter");
        }
    }
}
