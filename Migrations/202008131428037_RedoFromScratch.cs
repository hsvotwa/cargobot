﻿namespace CargoBot.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RedoFromScratch : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tbl_app_user",
                c => new
                    {
                        AppUserID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Nickname = c.String(),
                        Phone = c.String(nullable: false),
                        LastKnownLocation = c.String(),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.AppUserID);
            
            CreateTable(
                "dbo.tbl_lu_app_user_type",
                c => new
                    {
                        AppUserTypeID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.AppUserTypeID);
            
            CreateTable(
                "dbo.tbl_business_admin",
                c => new
                    {
                        BusinessAdminID = c.Guid(nullable: false),
                        BusinessAdminStatusID = c.Int(nullable: false),
                        AppUserID = c.Guid(nullable: false),
                        BusinessID = c.Guid(nullable: false),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.BusinessAdminID)
                .ForeignKey("dbo.tbl_app_user", t => t.AppUserID)
                .ForeignKey("dbo.tbl_business", t => t.BusinessID)
                .ForeignKey("dbo.tbl_lu_business_admin_status", t => t.BusinessAdminStatusID)
                .Index(t => t.BusinessAdminStatusID)
                .Index(t => t.AppUserID)
                .Index(t => t.BusinessID);
            
            CreateTable(
                "dbo.tbl_business",
                c => new
                    {
                        BusinessID = c.Guid(nullable: false),
                        BusinessCode = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Phone = c.String(nullable: false),
                        Address = c.String(),
                        StatusID = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.BusinessID)
                .ForeignKey("dbo.tbl_lu_business_status", t => t.StatusID)
                .Index(t => t.StatusID);
            
            CreateTable(
                "dbo.tbl_lu_business_status",
                c => new
                    {
                        BusinessStatusID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.BusinessStatusID);
            
            CreateTable(
                "dbo.tbl_lu_business_admin_status",
                c => new
                    {
                        BusinessAdminStatusID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.BusinessAdminStatusID);
            
            CreateTable(
                "dbo.tbl_message",
                c => new
                    {
                        MessageID = c.Int(nullable: false, identity: true),
                        TwilioMessageID = c.String(nullable: false),
                        Nickname = c.String(),
                        SenderPhoneNumber = c.String(nullable: false),
                        DateAndTime = c.DateTime(nullable: false),
                        IsValid = c.Boolean(nullable: false),
                        IsTemplateResponse = c.Boolean(nullable: false),
                        Cost = c.Double(nullable: false),
                        BodyReceived = c.String(),
                        BodySent = c.String(),
                        ProcessID = c.Int(nullable: false),
                        SubProcessID = c.Int(nullable: false),
                        ResponseMessageTemplateID = c.Int(nullable: false),
                        TripID = c.Guid(),
                        ParcelID = c.Guid(),
                        AppUserTypeID = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.MessageID)
                .ForeignKey("dbo.tbl_lu_app_user_type", t => t.AppUserTypeID)
                .ForeignKey("dbo.tbl_parcel", t => t.ParcelID)
                .ForeignKey("dbo.tbl_process", t => t.ProcessID)
                .ForeignKey("dbo.tbl_response_message_template", t => t.ResponseMessageTemplateID)
                .ForeignKey("dbo.tbl_trip", t => t.TripID)
                .Index(t => t.ProcessID)
                .Index(t => t.ResponseMessageTemplateID)
                .Index(t => t.TripID)
                .Index(t => t.ParcelID)
                .Index(t => t.AppUserTypeID);
            
            CreateTable(
                "dbo.tbl_parcel",
                c => new
                    {
                        ParcelID = c.Guid(nullable: false),
                        ParcelCode = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        SenderAppUserID = c.Guid(nullable: false),
                        RecipientAppUserID = c.Guid(nullable: false),
                        ParcelStatusID = c.Int(nullable: false),
                        TripID = c.Guid(nullable: false),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ParcelID)
                .ForeignKey("dbo.tbl_lu_parcel_status", t => t.ParcelStatusID)
                .ForeignKey("dbo.tbl_app_user", t => t.RecipientAppUserID)
                .ForeignKey("dbo.tbl_app_user", t => t.SenderAppUserID)
                .ForeignKey("dbo.tbl_trip", t => t.TripID)
                .Index(t => t.SenderAppUserID)
                .Index(t => t.RecipientAppUserID)
                .Index(t => t.ParcelStatusID)
                .Index(t => t.TripID);
            
            CreateTable(
                "dbo.tbl_lu_parcel_status",
                c => new
                    {
                        ParcelStatusID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ParcelStatusID);
            
            CreateTable(
                "dbo.tbl_trip",
                c => new
                    {
                        TripID = c.Guid(nullable: false),
                        TripCode = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        AppUserID = c.Guid(nullable: false),
                        BusinessID = c.Guid(nullable: false),
                        TripStatusID = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.TripID)
                .ForeignKey("dbo.tbl_app_user", t => t.AppUserID)
                .ForeignKey("dbo.tbl_business", t => t.BusinessID)
                .ForeignKey("dbo.tbl_lu_trip_status", t => t.TripStatusID)
                .Index(t => t.AppUserID)
                .Index(t => t.BusinessID)
                .Index(t => t.TripStatusID);
            
            CreateTable(
                "dbo.tbl_lu_trip_status",
                c => new
                    {
                        TripStatusID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.TripStatusID);
            
            CreateTable(
                "dbo.tbl_process",
                c => new
                    {
                        ProcessID = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ProcessID);
            
            CreateTable(
                "dbo.tbl_response_message_template",
                c => new
                    {
                        ResponseMessageTemplateID = c.Int(nullable: false),
                        MessageID = c.Int(nullable: false),
                        PrefixText = c.String(),
                        HasStaticOptions = c.Boolean(nullable: false),
                        HasDynamicOptions = c.Boolean(nullable: false),
                        DynamicListTypeID = c.Int(),
                        SuffixText = c.String(),
                        HasBackOption = c.Boolean(nullable: false),
                        BackOptionText = c.String(),
                        BackOptionProcessID = c.Int(),
                        Description = c.String(nullable: false),
                        AppUserTypeID = c.Int(nullable: false),
                        MessageTypeID = c.Int(),
                        StatusID = c.Int(),
                    })
                .PrimaryKey(t => t.ResponseMessageTemplateID)
                .ForeignKey("dbo.tbl_process", t => t.BackOptionProcessID)
                .ForeignKey("dbo.tbl_lu_dynamic_list_type", t => t.DynamicListTypeID)
                .ForeignKey("dbo.tbl_lu_app_user_type", t => t.AppUserTypeID)
                .ForeignKey("dbo.tbl_lu_message_type", t => t.MessageTypeID)
                .ForeignKey("dbo.tbl_lu_response_message_template_status", t => t.StatusID)
                .Index(t => t.DynamicListTypeID)
                .Index(t => t.BackOptionProcessID)
                .Index(t => t.AppUserTypeID)
                .Index(t => t.MessageTypeID)
                .Index(t => t.StatusID);
            
            CreateTable(
                "dbo.tbl_lu_dynamic_list_type",
                c => new
                    {
                        DynamicListTypeID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.DynamicListTypeID);
            
            CreateTable(
                "dbo.tbl_lu_message_type",
                c => new
                    {
                        MessageTypeID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.MessageTypeID);
            
            CreateTable(
                "dbo.tbl_lu_response_message_template_status",
                c => new
                    {
                        StatusID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.StatusID);
            
            CreateTable(
                "dbo.tbl_dynamic_message_option",
                c => new
                    {
                        MessageOptionID = c.Int(nullable: false),
                        MessageTemplateID = c.Int(nullable: false),
                        OptionValueID = c.String(nullable: false),
                        RelatedTableKey = c.String(),
                        Text = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.MessageOptionID)
                .ForeignKey("dbo.tbl_response_message_template", t => t.MessageTemplateID)
                .Index(t => t.MessageTemplateID);
            
            CreateTable(
                "dbo.tbl_parcel_image",
                c => new
                    {
                        ParcelImageID = c.Guid(nullable: false),
                        ImageUrl = c.String(nullable: false),
                        ParcelID = c.Guid(nullable: false),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ParcelImageID)
                .ForeignKey("dbo.tbl_parcel", t => t.ParcelID)
                .Index(t => t.ParcelID);
            
            CreateTable(
                "dbo.tbl_process_sequence",
                c => new
                    {
                        ProcessSequenceID = c.Int(nullable: false),
                        ProcessID = c.Int(nullable: false),
                        NextProcessID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProcessSequenceID)
                .ForeignKey("dbo.tbl_process", t => t.NextProcessID)
                .ForeignKey("dbo.tbl_process", t => t.ProcessID)
                .Index(t => t.ProcessID)
                .Index(t => t.NextProcessID);
            
            CreateTable(
                "dbo.tbl_process_template",
                c => new
                    {
                        ProcessTemplateID = c.Int(nullable: false),
                        ProcessID = c.Int(nullable: false),
                        SubProcessID = c.Int(nullable: false),
                        SuccessMessageID = c.Int(),
                        FailedMessageID = c.Int(),
                        OnSuccessMessageOthers = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ProcessTemplateID)
                .ForeignKey("dbo.tbl_process", t => t.ProcessID)
                .Index(t => t.ProcessID);
            
            CreateTable(
                "dbo.tbl_static_message_option",
                c => new
                    {
                        StaticMessageOptionID = c.Int(nullable: false),
                        MessageTemplateID = c.Int(nullable: false),
                        OptionValueID = c.String(nullable: false),
                        OptionNextProcessID = c.Int(nullable: false),
                        OptionNextSubProcessID = c.Int(nullable: false),
                        Text = c.String(nullable: false),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.StaticMessageOptionID)
                .ForeignKey("dbo.tbl_response_message_template", t => t.MessageTemplateID)
                .ForeignKey("dbo.tbl_process", t => t.OptionNextProcessID)
                .Index(t => t.MessageTemplateID)
                .Index(t => t.OptionNextProcessID);
            
            CreateTable(
                "dbo.tbl_vehicle_checkin",
                c => new
                    {
                        VehicleCheckInID = c.Guid(nullable: false),
                        DateAndTime = c.DateTime(nullable: false),
                        Location = c.String(nullable: false),
                        AppUserID = c.Guid(nullable: false),
                        TripID = c.Guid(nullable: false),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.VehicleCheckInID)
                .ForeignKey("dbo.tbl_app_user", t => t.AppUserID)
                .ForeignKey("dbo.tbl_trip", t => t.TripID)
                .Index(t => t.AppUserID)
                .Index(t => t.TripID);
            
            CreateTable(
                "dbo.tbl_vehicle",
                c => new
                    {
                        VehicleID = c.Guid(nullable: false),
                        RegNo = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        BusinessID = c.Guid(nullable: false),
                        Created = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.VehicleID)
                .ForeignKey("dbo.tbl_business", t => t.BusinessID)
                .Index(t => t.BusinessID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.tbl_vehicle", "BusinessID", "dbo.tbl_business");
            DropForeignKey("dbo.tbl_vehicle_checkin", "TripID", "dbo.tbl_trip");
            DropForeignKey("dbo.tbl_vehicle_checkin", "AppUserID", "dbo.tbl_app_user");
            DropForeignKey("dbo.tbl_static_message_option", "OptionNextProcessID", "dbo.tbl_process");
            DropForeignKey("dbo.tbl_static_message_option", "MessageTemplateID", "dbo.tbl_response_message_template");
            DropForeignKey("dbo.tbl_process_template", "ProcessID", "dbo.tbl_process");
            DropForeignKey("dbo.tbl_process_sequence", "ProcessID", "dbo.tbl_process");
            DropForeignKey("dbo.tbl_process_sequence", "NextProcessID", "dbo.tbl_process");
            DropForeignKey("dbo.tbl_parcel_image", "ParcelID", "dbo.tbl_parcel");
            DropForeignKey("dbo.tbl_dynamic_message_option", "MessageTemplateID", "dbo.tbl_response_message_template");
            DropForeignKey("dbo.tbl_message", "TripID", "dbo.tbl_trip");
            DropForeignKey("dbo.tbl_message", "ResponseMessageTemplateID", "dbo.tbl_response_message_template");
            DropForeignKey("dbo.tbl_response_message_template", "StatusID", "dbo.tbl_lu_response_message_template_status");
            DropForeignKey("dbo.tbl_response_message_template", "MessageTypeID", "dbo.tbl_lu_message_type");
            DropForeignKey("dbo.tbl_response_message_template", "AppUserTypeID", "dbo.tbl_lu_app_user_type");
            DropForeignKey("dbo.tbl_response_message_template", "DynamicListTypeID", "dbo.tbl_lu_dynamic_list_type");
            DropForeignKey("dbo.tbl_response_message_template", "BackOptionProcessID", "dbo.tbl_process");
            DropForeignKey("dbo.tbl_message", "ProcessID", "dbo.tbl_process");
            DropForeignKey("dbo.tbl_message", "ParcelID", "dbo.tbl_parcel");
            DropForeignKey("dbo.tbl_parcel", "TripID", "dbo.tbl_trip");
            DropForeignKey("dbo.tbl_trip", "TripStatusID", "dbo.tbl_lu_trip_status");
            DropForeignKey("dbo.tbl_trip", "BusinessID", "dbo.tbl_business");
            DropForeignKey("dbo.tbl_trip", "AppUserID", "dbo.tbl_app_user");
            DropForeignKey("dbo.tbl_parcel", "SenderAppUserID", "dbo.tbl_app_user");
            DropForeignKey("dbo.tbl_parcel", "RecipientAppUserID", "dbo.tbl_app_user");
            DropForeignKey("dbo.tbl_parcel", "ParcelStatusID", "dbo.tbl_lu_parcel_status");
            DropForeignKey("dbo.tbl_message", "AppUserTypeID", "dbo.tbl_lu_app_user_type");
            DropForeignKey("dbo.tbl_business_admin", "BusinessAdminStatusID", "dbo.tbl_lu_business_admin_status");
            DropForeignKey("dbo.tbl_business_admin", "BusinessID", "dbo.tbl_business");
            DropForeignKey("dbo.tbl_business", "StatusID", "dbo.tbl_lu_business_status");
            DropForeignKey("dbo.tbl_business_admin", "AppUserID", "dbo.tbl_app_user");
            DropIndex("dbo.tbl_vehicle", new[] { "BusinessID" });
            DropIndex("dbo.tbl_vehicle_checkin", new[] { "TripID" });
            DropIndex("dbo.tbl_vehicle_checkin", new[] { "AppUserID" });
            DropIndex("dbo.tbl_static_message_option", new[] { "OptionNextProcessID" });
            DropIndex("dbo.tbl_static_message_option", new[] { "MessageTemplateID" });
            DropIndex("dbo.tbl_process_template", new[] { "ProcessID" });
            DropIndex("dbo.tbl_process_sequence", new[] { "NextProcessID" });
            DropIndex("dbo.tbl_process_sequence", new[] { "ProcessID" });
            DropIndex("dbo.tbl_parcel_image", new[] { "ParcelID" });
            DropIndex("dbo.tbl_dynamic_message_option", new[] { "MessageTemplateID" });
            DropIndex("dbo.tbl_response_message_template", new[] { "StatusID" });
            DropIndex("dbo.tbl_response_message_template", new[] { "MessageTypeID" });
            DropIndex("dbo.tbl_response_message_template", new[] { "AppUserTypeID" });
            DropIndex("dbo.tbl_response_message_template", new[] { "BackOptionProcessID" });
            DropIndex("dbo.tbl_response_message_template", new[] { "DynamicListTypeID" });
            DropIndex("dbo.tbl_trip", new[] { "TripStatusID" });
            DropIndex("dbo.tbl_trip", new[] { "BusinessID" });
            DropIndex("dbo.tbl_trip", new[] { "AppUserID" });
            DropIndex("dbo.tbl_parcel", new[] { "TripID" });
            DropIndex("dbo.tbl_parcel", new[] { "ParcelStatusID" });
            DropIndex("dbo.tbl_parcel", new[] { "RecipientAppUserID" });
            DropIndex("dbo.tbl_parcel", new[] { "SenderAppUserID" });
            DropIndex("dbo.tbl_message", new[] { "AppUserTypeID" });
            DropIndex("dbo.tbl_message", new[] { "ParcelID" });
            DropIndex("dbo.tbl_message", new[] { "TripID" });
            DropIndex("dbo.tbl_message", new[] { "ResponseMessageTemplateID" });
            DropIndex("dbo.tbl_message", new[] { "ProcessID" });
            DropIndex("dbo.tbl_business", new[] { "StatusID" });
            DropIndex("dbo.tbl_business_admin", new[] { "BusinessID" });
            DropIndex("dbo.tbl_business_admin", new[] { "AppUserID" });
            DropIndex("dbo.tbl_business_admin", new[] { "BusinessAdminStatusID" });
            DropTable("dbo.tbl_vehicle");
            DropTable("dbo.tbl_vehicle_checkin");
            DropTable("dbo.tbl_static_message_option");
            DropTable("dbo.tbl_process_template");
            DropTable("dbo.tbl_process_sequence");
            DropTable("dbo.tbl_parcel_image");
            DropTable("dbo.tbl_dynamic_message_option");
            DropTable("dbo.tbl_lu_response_message_template_status");
            DropTable("dbo.tbl_lu_message_type");
            DropTable("dbo.tbl_lu_dynamic_list_type");
            DropTable("dbo.tbl_response_message_template");
            DropTable("dbo.tbl_process");
            DropTable("dbo.tbl_lu_trip_status");
            DropTable("dbo.tbl_trip");
            DropTable("dbo.tbl_lu_parcel_status");
            DropTable("dbo.tbl_parcel");
            DropTable("dbo.tbl_message");
            DropTable("dbo.tbl_lu_business_admin_status");
            DropTable("dbo.tbl_lu_business_status");
            DropTable("dbo.tbl_business");
            DropTable("dbo.tbl_business_admin");
            DropTable("dbo.tbl_lu_app_user_type");
            DropTable("dbo.tbl_app_user");
        }
    }
}
