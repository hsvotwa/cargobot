﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CargoBot.Models;
using CargoBot.Repositories;

namespace CargoBot.Controllers {
    public class BusinessController : Controller {
        private UnitOfWork g_UnitOfWork = new UnitOfWork( new CargoBotContext() );

        [HttpGet]
        public ActionResult GetAll( string _sNameLike ) {
            string _sErrorMessage = string.Empty;
            try {
                string _sBusinesses = string.Empty;
                IEnumerable<Business> _Businesses = g_UnitOfWork.Businesses.getAll().Where( _Rec => _Rec.Name.Contains( _sNameLike ) ).OrderBy( _Rec => _Rec.Name );
                foreach ( Business _Business in _Businesses ) {
                    _sBusinesses += $"Name: {_Business.Name} and Status: {_Business.BusinessStatus.Name} >>> \n\n";
                }
                return Content( _sBusinesses );
            } catch ( Exception _Ex ) {
                _sErrorMessage = _Ex.Message;
            }
            return Content( "No records." );
        }

        [HttpGet]
        public ActionResult Get( Guid ID ) {
            string _sErrorMessage = string.Empty;
            try {
                Business _Business = g_UnitOfWork.Businesses.getById( ID );
                if ( _Business == null ) {
                    return Content( "Business not found!" );
                }
                return Content( $"Name: {_Business.Name} and Status: {_Business.BusinessStatus.Name} >>> " );
            } catch ( Exception _Ex ) {
                _sErrorMessage = _Ex.Message;
            }
            return Content( _sErrorMessage );
        }

        [HttpGet]
        public ActionResult Delete( Guid ID ) {
            string _sErrorMessage = string.Empty;
            try {
                Business _Business = g_UnitOfWork.Businesses.getById( ID );
                if ( _Business == null ) {
                    return Content( "Business not found!" );
                }
                g_UnitOfWork.Businesses.delete( _Business );
                if ( g_UnitOfWork.commit( out _sErrorMessage ) ) {
                    return Content( "Deleted. Nice!" );
                }
            } catch ( Exception _Ex ) {
                _sErrorMessage = _Ex.Message;
            }
            return Content( _sErrorMessage );
        }

        [HttpPost]
        public ActionResult Set( Business _Business ) {
            string _sErrorMessage = string.Empty;
            try {
                //Dummy code to create test data
                //g_UnitOfWork.Businesses.insertOrUpdate( new Business {
                //    Name = "Business Name",
                //    BusinessCode = "CodeXXXX",
                //    Address = "Address",
                //    Phone = "Phone",
                //    BusinessID = Guid.NewGuid(),
                //    StatusID = ( int )EnumBusinessStatus.active
                //} );
                //g_UnitOfWork.commit( out _sErrorMessage );

                if ( !ModelState.IsValid ) {
                    return Content( "Invalid Model" );
                }
                g_UnitOfWork.Businesses.insertOrUpdate( _Business );
                if ( g_UnitOfWork.commit( out _sErrorMessage ) ) {
                    return Content( "Saved. Nice!" );
                }
            } catch ( Exception _Ex ) {
                _sErrorMessage += _Ex.Message;
            }
            return Content( _sErrorMessage );
        }
    }
}