﻿using CargoBot.Classes;
using System.Web.Mvc;
using Twilio.AspNet.Common;
using Twilio.AspNet.Mvc;
using Twilio.TwiML;

namespace CargoBot.Controllers {
    public class MessageController : TwilioController {
        public TwiMLResult Index( SmsRequest _IncomingMessage ) {
            //MessagingResponse _MessagingResponse = new MessagingResponse();
            MessagingResponse _MessagingResponse = new ConversationHandler( _IncomingMessage, _bSimulating: false ).handle( out string _sMessageSent );
            //_MessagingResponse.Message(
            //        "The copy cat says: " +
            //        $"\"{_IncomingMessage.Body}\" from {_IncomingMessage.From}."
            //    );
            return TwiML( _MessagingResponse );
        }


        public ActionResult Simulate() {
            new ConversationHandler( new SmsRequest {
                AccountSid = "129103814810",
                Body = "Hey",
                From = "+264810365066",
                SmsSid = "11111129103814810"
            }, _bSimulating: true ).handle( out string _sMessageSent );
            return Content( _sMessageSent );
        }
    }
}