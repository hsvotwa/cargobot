﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using CargoBot.Models;

namespace CargoBot.Repositories
{
    public class CargoBotContext : DbContext
    {
        public CargoBotContext() : base(Constant.CON_STRING)
        {
        }

        public DbSet<BusinessStatus> BusinessStatuses { get; }
        public DbSet<BusinessAdminStatus> BusinessAdminStatuses { get; }
        public DbSet<AppUser> AppUsers { get; }
        public DbSet<Business> Businesses { get; }
        public DbSet<BusinessAdmin> BusinessAdmins { get; }
        public DbSet<TripStatus> TripStatuses { get; }
        public DbSet<Vehicle> Vehicles { get; }
        public DbSet<Trip> Trips { get; }
        public DbSet<Parcel> Parcels { get; }
        public DbSet<ParcelImage> ParcelImages { get; }
        public DbSet<VehicleCheckIn> VehicleCheckIns { get; }
        public DbSet<Message> ConversationLogs { get; }
        public DbSet<ParcelStatus> ParcelStatuses { get; }
        public DbSet<AppUserType> AppUserTypes { get; }
        public DbSet<ResponseMessageTemplate> ResponseMessageTemplates { get; }
        public DbSet<Process> Processes { get; }
        public DbSet<ProcessTemplate> ProcessTemplates { get; }
        public DbSet<ProcessSequence> ProcessSequences { get; }
        public DbSet<StaticMessageOption> StaticMessageOptions { get; }
        public DbSet<DynamicMessageOption> DynamicMessageOptions { get; }
        public DbSet<DynamicListType> DynamicListTypes { get; }
        public DbSet<MessageType> MessageTypes { get; }
        public DbSet<ResponseMessageTemplateStatus> ResponseMessageTemplateStatuses { get; }
        public DbSet<Transporter> Transporters { get; }


        protected override void OnModelCreating(DbModelBuilder _DbModelBuilder)
        {
            _DbModelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            _DbModelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public override int SaveChanges()
        {
            var _vEntries = ChangeTracker.Entries().Where(_Entry => _Entry.Entity is HasCreatedAndLastModified && (
                        _Entry.State == EntityState.Added
                        || _Entry.State == EntityState.Modified
                   )
            );
            foreach (var _vEntityEntry in _vEntries)
            {
                ((HasCreatedAndLastModified)_vEntityEntry.Entity).LastModified = DateTime.Now;
                if (_vEntityEntry.State == EntityState.Added)
                {
                    ((HasCreatedAndLastModified)_vEntityEntry.Entity).Created = DateTime.Now;
                }
            }
            return base.SaveChanges();
        }
    }
}