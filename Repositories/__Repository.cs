﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;

namespace CargoBot.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected DbSet<T> g_DbSet;
        private CargoBotContext g_DataContext;

        public Repository(CargoBotContext _gtBestDealsBackendContext)
        {
            g_DataContext = _gtBestDealsBackendContext;
            g_DbSet = g_DataContext.Set<T>();
        }

        #region IRepository<T> Members
        public void insert(T _Entity)
        {
            g_DbSet.Add(_Entity);
        }

        public void update(T _Entity)
        {
            //g_DataContext.Entry( _Entity ).State = EntityState.Modified;
            g_DbSet.AddOrUpdate(_Entity);
        }

        public void insertOrUpdate(T _Entity)
        {
            g_DbSet.AddOrUpdate(_Entity);
        }

        public void delete(T _Entity)
        {
            if (g_DataContext.Entry(_Entity).State == EntityState.Detached)
            {
                g_DbSet.Attach(_Entity);
            }
            g_DbSet.Remove(_Entity);
        }

        public void deleteAll()
        { //For lookups
            g_DbSet.RemoveRange(getAll());
        }

        public IQueryable<T> searchFor(Expression<Func<T, bool>> _Predicate)
        {
            return g_DbSet.Where(_Predicate);
        }

        public IQueryable<T> getAll()
        {
            return g_DbSet;
        }

        public virtual IEnumerable<T> getFiltered(Expression<Func<T, bool>> _Filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> _OrderBy = null, string _sIncludeProperties = "")
        {
            try
            {
                IQueryable<T> _Query = g_DbSet;
                if (_Filter != null)
                {
                    _Query = _Query.Where(_Filter);
                }
                if (_sIncludeProperties != null)
                {
                    foreach (var _vIncludeProperty in _sIncludeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        _Query = _Query.Include(_vIncludeProperty);
                    }
                }
                if (_OrderBy != null)
                {
                    return _OrderBy(_Query).ToList();
                }
                else
                {
                    return _Query.ToList();
                }
            }
            catch (Exception _Ex)
            {
            }
            return null;
        }

        public T getById(Guid _gID)
        {
            return g_DbSet.Find(_gID);
        }

        public virtual IEnumerable<T> getWithRawSql(string _sQuery, params object[] _oParameters)
        {
            return g_DbSet.SqlQuery(_sQuery, _oParameters).ToList();
        }
        #endregion
    }
}