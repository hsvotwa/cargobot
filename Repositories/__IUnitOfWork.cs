﻿using CargoBot.Models;

namespace CargoBot.Repositories {
    public interface IUnitOfWork {
        IRepository<BusinessStatus> BusinessStatuses { get; }
        IRepository<BusinessAdminStatus> BusinessAdminStatuses { get; }
        IRepository<AppUser> AppUsers { get; }
        IRepository<Business> Businesses { get; }
        IRepository<BusinessAdmin> BusinessAdmins { get; }
        IRepository<TripStatus> TripStatuses { get; }
        IRepository<Vehicle> Vehicles { get; }
        IRepository<Trip> Trips { get; }
        IRepository<Parcel> Parcels { get; }
        IRepository<ParcelImage> ParcelImages { get; }
        IRepository<VehicleCheckIn> VehicleCheckIns { get; }
        IRepository<Message> ConversationLogs { get; }
        IRepository<ParcelStatus> ParcelStatuses { get; }
        IRepository<AppUserType> AppUserTypes { get; }
        IRepository<ResponseMessageTemplate> ResponseMessageTemplates { get; }
        IRepository<Process> Processes { get; }
        IRepository<ProcessTemplate> ProcessTemplates { get; }
        IRepository<ProcessSequence> ProcessSequences { get; }
        IRepository<StaticMessageOption> StaticMessageOptions { get; }
        IRepository<DynamicMessageOption> DynamicMessageOptions { get; }
        IRepository<DynamicListType> DynamicListTypes { get; }
        IRepository<MessageType> MessageTypes { get; }
        IRepository<ResponseMessageTemplateStatus> ResponseMessageTemplateStatuses { get; }
        IRepository<Transporter> Transporters { get; }

        bool commit( out string _sError );
    }
}