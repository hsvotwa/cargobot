﻿using System;
using System.Data.Entity.Validation;
using CargoBot.Models;

namespace CargoBot.Repositories {
    public class UnitOfWork : IUnitOfWork {
        private readonly CargoBotContext g_dbContext;

        private Repository<BusinessStatus> _BusinessStatuses;
        private Repository<BusinessAdminStatus> _BusinessAdminStatuses;
        private Repository<AppUser> _AppUsers;
        private Repository<Business> _Businesses;
        private Repository<BusinessAdmin> _BusinessAdmins;
        private Repository<TripStatus> _TripStatuses;
        private Repository<Vehicle> _Vehicles;
        private Repository<Trip> _Trips;
        private Repository<Parcel> _Parcels;
        private Repository<ParcelImage> _ParcelImages;
        private Repository<VehicleCheckIn> _VehicleCheckIns;
        private Repository<Message> _ConversationLogs;
        private Repository<ParcelStatus> _ParcelStatuses;
        private Repository<AppUserType> _AppUserTypes;
        private Repository<ResponseMessageTemplate> _ResponseMessageTemplates;
        private Repository<Process> _Processes;
        private Repository<ProcessTemplate> _ProcessTemplates;
        private Repository<ProcessSequence> _ProcessSequences;
        private Repository<DynamicMessageOption> _DynamicMessageOptions;
        private Repository<StaticMessageOption> _StaticMessageOptions;
        private Repository<DynamicListType> _DynamicListTypes;
        private Repository<MessageType> _MessageTypes;
        private Repository<ResponseMessageTemplateStatus> _ResponseMessageTemplateStatuses;
        private Repository<Transporter> _Transporters;

        public UnitOfWork( CargoBotContext _dbContext ) {
            g_dbContext = _dbContext;
        }

        public IRepository<AppUserType> AppUserTypes {
            get {
                return _AppUserTypes ?? ( _AppUserTypes = new Repository<AppUserType>( g_dbContext ) );
            }
        }

        public IRepository<AppUser> AppUsers {
            get {
                return _AppUsers ?? ( _AppUsers = new Repository<AppUser>( g_dbContext ) );
            }
        }

        public IRepository<Business> Businesses {
            get {
                return _Businesses ?? ( _Businesses = new Repository<Business>( g_dbContext ) );
            }
        }

        public IRepository<ParcelStatus> ParcelStatuses {
            get {
                return _ParcelStatuses ?? ( _ParcelStatuses = new Repository<ParcelStatus>( g_dbContext ) );
            }
        }

        public IRepository<Message> ConversationLogs {
            get {
                return _ConversationLogs ?? ( _ConversationLogs = new Repository<Message>( g_dbContext ) );
            }
        }

        public IRepository<VehicleCheckIn> VehicleCheckIns {
            get {
                return _VehicleCheckIns ?? ( _VehicleCheckIns = new Repository<VehicleCheckIn>( g_dbContext ) );
            }
        }

        public IRepository<ParcelImage> ParcelImages {
            get {
                return _ParcelImages ?? ( _ParcelImages = new Repository<ParcelImage>( g_dbContext ) );
            }
        }

        public IRepository<Parcel> Parcels {
            get {
                return _Parcels ?? ( _Parcels = new Repository<Parcel>( g_dbContext ) );
            }
        }

        public IRepository<BusinessAdmin> BusinessAdmins {
            get {
                return _BusinessAdmins ?? ( _BusinessAdmins = new Repository<BusinessAdmin>( g_dbContext ) );
            }
        }

        public IRepository<Trip> Trips {
            get {
                return _Trips ?? ( _Trips = new Repository<Trip>( g_dbContext ) );
            }

        }

        public IRepository<Vehicle> Vehicles {
            get {
                return _Vehicles ?? ( _Vehicles = new Repository<Vehicle>( g_dbContext ) );
            }
        }

        public IRepository<TripStatus> TripStatuses {
            get {
                return _TripStatuses ?? ( _TripStatuses = new Repository<TripStatus>( g_dbContext ) );
            }
        }

        public IRepository<BusinessStatus> BusinessStatuses {
            get {
                return _BusinessStatuses ?? ( _BusinessStatuses = new Repository<BusinessStatus>( g_dbContext ) );
            }
        }

        public IRepository<BusinessAdminStatus> BusinessAdminStatuses {
            get {
                return _BusinessAdminStatuses ?? ( _BusinessAdminStatuses = new Repository<BusinessAdminStatus>( g_dbContext ) );
            }
        }

        public IRepository<Process> Processes {
            get {
                return _Processes ?? ( _Processes = new Repository<Process>( g_dbContext ) );
            }
        }

        public IRepository<ProcessTemplate> ProcessTemplates {
            get {
                return _ProcessTemplates ?? ( _ProcessTemplates = new Repository<ProcessTemplate>( g_dbContext ) );
            }
        }
        public IRepository<ProcessSequence> ProcessSequences {
            get {
                return _ProcessSequences ?? ( _ProcessSequences = new Repository<ProcessSequence>( g_dbContext ) );
            }
        }

        public IRepository<StaticMessageOption> StaticMessageOptions {
            get {
                return _StaticMessageOptions ?? ( _StaticMessageOptions = new Repository<StaticMessageOption>( g_dbContext ) );
            }
        }

        public IRepository<DynamicMessageOption> DynamicMessageOptions {
            get {
                return _DynamicMessageOptions ?? ( _DynamicMessageOptions = new Repository<DynamicMessageOption>( g_dbContext ) );
            }
        }

        public IRepository<ResponseMessageTemplate> ResponseMessageTemplates {
            get {
                return _ResponseMessageTemplates ?? ( _ResponseMessageTemplates = new Repository<ResponseMessageTemplate>( g_dbContext ) );
            }
        }
        public IRepository<DynamicListType> DynamicListTypes {
            get {
                return _DynamicListTypes ?? ( _DynamicListTypes = new Repository<DynamicListType>( g_dbContext ) );
            }
        }

        public IRepository<MessageType> MessageTypes {
            get {
                return _MessageTypes ?? ( _MessageTypes = new Repository<MessageType>( g_dbContext ) );
            }
        }
        public IRepository<ResponseMessageTemplateStatus> ResponseMessageTemplateStatuses {
            get {
                return _ResponseMessageTemplateStatuses ?? ( _ResponseMessageTemplateStatuses = new Repository<ResponseMessageTemplateStatus>( g_dbContext ) );
            }
        }
        public IRepository<Transporter> Transporters {
            get {
                return _Transporters ?? (_Transporters = new Repository<Transporter>( g_dbContext ) );
            }
        }

        public bool commit( out string _sError ) {
            _sError = string.Empty;
            try {
                return g_dbContext.SaveChanges() > 0;
            } catch ( Exception _Ex ) {
                _sError += $"{ _Ex.Message}";
                if ( _Ex.GetType() == typeof( DbEntityValidationException ) ) {
                    _sError += ". Validation errors: ";
                    DbEntityValidationException _DbEntityValidationException = _Ex as DbEntityValidationException;
                    foreach ( var _vEve in _DbEntityValidationException.EntityValidationErrors ) {
                        _sError += string.Format( "Entity type \"{0}\" in state \"{1}\" has the following validation errors:", _vEve.Entry.Entity.GetType().Name, _vEve.Entry.State );
                        foreach ( var _vEveProp in _vEve.ValidationErrors ) {
                            _sError += string.Format( "- Property: \"{0}\", Error: \"{1}\"", _vEveProp.PropertyName, _vEveProp.ErrorMessage );
                        }
                    }
                    //throw;
                }
            }
            return false;
        }
    }
}