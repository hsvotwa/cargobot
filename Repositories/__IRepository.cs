﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CargoBot.Repositories {
    public interface IRepository<T> {
        void insert(T _Entity);
        void update(T _Entity);
        void insertOrUpdate( T _Entity);
        void delete(T _Entity);
        IQueryable<T> searchFor(Expression<Func<T, bool>> _Predicate);
        IQueryable<T> getAll();
        void deleteAll();
        IEnumerable<T> getFiltered(Expression<Func<T, bool>> _Filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> _OrderBy = null, string _sIncludeProperties = "");
        IEnumerable<T> getWithRawSql(string _sQuery, params object[] _oParameters);
        T getById(Guid ID);
    }
}