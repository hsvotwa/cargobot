﻿using CargoBot.Repositories;

namespace CargoBot.Models {
    public enum EnumAppProcessMain {
        home_or_none = 1,
        //welcome message
        welcome_message = 2,
        //app conversation starter
        parcel_created_notification = 3,
        //registration
        user_registration = 4,
        super_admin_register_business = 5,
        business_admin_register_business_admin = 6,
        //trip
        business_admin_create_trip = 7,
        business_admin_add_vehicle = 8,
        business_admin_add_transporter = 9,
        business_admin_add_parcel_sender = 10,
        //parcel
        parcel_sender_add_parcel_receiver = 11,
        parcel_sender_send_parcel = 12,
        //parcel tracking
        parcel_tracking = 13,
        //driver check in
        driver_check_in = 14,
        app_info = 15
    }

    #region welcome
    public enum EnumWelcomeSub {
        greeting_and_menu = 1,
    }
    #endregion

    #region conversation initiation
    public enum EnumParcelCreatedInitiationSub {
        parcel_details_and_how_to_track = 1,
    }
    #endregion

    #region registration
    public enum EnumUserRegistrationSub {
        add_name = 1,
        add_nickname = 2,
    }
    public enum EnumSuperAdminRegisterBusinessSub {
        add_name = 1,
        add_phone = 2,
        add_address = 3
    }
    public enum EnumBusinessAdminRegisterBusinessAdminSub {
        add_name = 1,
        add_phone = 2
    }
    #endregion

    #region trip
    public enum EnumBusinessAdminCreateTripSub {
        choose_vehicle = 1,
        choose_driver = 2,
    }
    public enum EnumBusinessAdminAddVehicleSub {
        add_registration = 1,
        add_description = 2
    }
    public enum EnumBusinessAdminAddTransporterSub {
        enter_name = 1,
        enter_phone = 2,
    }
    public enum EnumBusinessAdminAddParcelSenderSub {
        choose_trip = 1,
        add_phone = 2,
    }
    #endregion

    #region parcel
    public enum EnumParcelSenderSendParcelSub {
        choose_parcel_receiver = 1,
        add_parcel_description_and_image = 2
    }
    public enum EnumAppInfoSub {
        display_info = 1,
    }
    #endregion
    public class AppProcess {
        public static bool initProcessAndSubProcessData( UnitOfWork g_UnitOfWork ) {
            g_UnitOfWork.Processes.insertOrUpdate( new Process { Name = "home_or_none", ProcessID = ( int )EnumAppProcessMain.home_or_none } );
            g_UnitOfWork.Processes.insertOrUpdate( new Process { Name = "welcome_message", ProcessID = ( int )EnumAppProcessMain.welcome_message } );
            g_UnitOfWork.Processes.insertOrUpdate( new Process { Name = "parcel_created_notification", ProcessID = ( int )EnumAppProcessMain.parcel_created_notification } );
            g_UnitOfWork.Processes.insertOrUpdate( new Process { Name = "user_registration", ProcessID = ( int )EnumAppProcessMain.user_registration } );
            g_UnitOfWork.Processes.insertOrUpdate( new Process { Name = "super_admin_register_business", ProcessID = ( int )EnumAppProcessMain.super_admin_register_business } );
            g_UnitOfWork.Processes.insertOrUpdate( new Process { Name = "business_admin_register_business_admin", ProcessID = ( int )EnumAppProcessMain.business_admin_register_business_admin } );
            g_UnitOfWork.Processes.insertOrUpdate( new Process { Name = "business_admin_create_trip", ProcessID = ( int )EnumAppProcessMain.business_admin_create_trip } );
            g_UnitOfWork.Processes.insertOrUpdate( new Process { Name = "business_admin_add_vehicle", ProcessID = ( int )EnumAppProcessMain.business_admin_add_vehicle } );
            g_UnitOfWork.Processes.insertOrUpdate( new Process { Name = "business_admin_add_driver", ProcessID = ( int )EnumAppProcessMain.business_admin_add_transporter } );
            g_UnitOfWork.Processes.insertOrUpdate( new Process { Name = "business_admin_add_parcel_sender", ProcessID = ( int )EnumAppProcessMain.business_admin_add_parcel_sender } );
            g_UnitOfWork.Processes.insertOrUpdate( new Process { Name = "parcel_sender_add_parcel_receiver", ProcessID = ( int )EnumAppProcessMain.parcel_sender_add_parcel_receiver } );
            g_UnitOfWork.Processes.insertOrUpdate( new Process { Name = "parcel_sender_send_parcel", ProcessID = ( int )EnumAppProcessMain.parcel_sender_send_parcel } );
            g_UnitOfWork.Processes.insertOrUpdate( new Process { Name = "parcel_tracking", ProcessID = ( int )EnumAppProcessMain.parcel_tracking } );
            g_UnitOfWork.Processes.insertOrUpdate( new Process { Name = "driver_check_in", ProcessID = ( int )EnumAppProcessMain.driver_check_in } );
            g_UnitOfWork.Processes.insertOrUpdate( new Process { Name = "app_info", ProcessID = ( int )EnumAppProcessMain.app_info } );
            return g_UnitOfWork.commit( out string _sErrorMessage );
        }
    }
}