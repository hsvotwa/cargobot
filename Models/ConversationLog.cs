﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CargoBot.Models
{
    [Table(SqlTable.TBL_CONVERSATION_LOG)]
    public class ConversationLog : HasCreatedAndLastModified
    {
        [Key] public int MessageID { get; set; }
        public string TwilioMessageID { get; set; }
        [Required] public DateTime DateAndTime { get; set; }
        [Required] public bool IsValid { get; set; }
        [Required] public bool IsTemplateResponse { get; set; }
        public double Cost { get; set; }
        public string BodyReceived { get; set; }
        public string BodySent { get; set; }

        [ForeignKey("MessageTemplate")]
        public int ResponseMessageTemplateID { get; set; }
        public virtual ResponseMessageTemplate MessageTemplate { get; set; }

        [ForeignKey("WAUser")]
        public Guid WAUserID { get; set; }
        public virtual AppUser WAUser { get; set; }

        [ForeignKey("UserType")]
        public int UserTypeID { get; set; }
        public virtual AppUserType UserType { get; set; }

        [ForeignKey("Parcel")]
        public int ParcelID { get; set; }
        public virtual Parcel Parcel { get; set; }

        [ForeignKey("Trip")]
        public Guid TripID { get; set; }
        public virtual Trip Trip { get; set; }

        [ForeignKey("Process")]
        public int ProcessID { get; set; }
        public virtual Process Process { get; set; }

        [ForeignKey("SubProcess")]
        public int SubProcessID { get; set; }
        public virtual Process SubProcess { get; set; }
    }
}