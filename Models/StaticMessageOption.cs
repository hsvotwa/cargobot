﻿using CargoBot.Repositories;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CargoBot.Models {
    [Table( SqlTable.TBL_STATIC_MESSAGE_OPTION )]
    public class StaticMessageOption : HasCreatedAndLastModified {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int StaticMessageOptionID { get; set; }

        [Required]
        [ForeignKey( "MessageTemplate" )]
        public int MessageTemplateID { get; set; }
        public virtual ResponseMessageTemplate MessageTemplate { get; set; }

        [Required] public string OptionValueID { get; set; }

        [ForeignKey( "Process" )]
        public int OptionNextProcessID { get; set; }
        public virtual Process Process { get; set; }

        //[ForeignKey( "SubProcess" )]
        public int OptionNextSubProcessID { get; set; }
        //public virtual Process SubProcess { get; set; }

        [Required] public string Text { get; set; }
        #region Business Admin Data
        private UnitOfWork g_UnitOfWork = new UnitOfWork( new CargoBotContext() );


        #endregion
    }
}