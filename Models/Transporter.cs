﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CargoBot.Models
{
    [Table(SqlTable.TBL_TRANSPORTER)]
    public class Transporter : HasCreatedAndLastModified
    {
        [Key] public Guid TransporterID { get; set; }

        [ForeignKey("BusinessAdminStatus")]
        public int BusinessAdminStatusID { get; set; }
        public virtual BusinessAdminStatus BusinessAdminStatus { get; set; }

        [ForeignKey("AppUser")]
        public Guid AppUserID { get; set; }
        public virtual AppUser AppUser { get; set; }

        [ForeignKey("Business")]
        public Guid BusinessID { get; set; }
        public virtual Business Business { get; set; }
    }
}