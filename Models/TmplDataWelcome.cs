﻿using CargoBot.Repositories;

namespace CargoBot.Models {
    public class TmplDataWelcome {
        private UnitOfWork g_UnitOfWork = new UnitOfWork( new CargoBotContext() );
        public int StaticMessageOptionID { get; set; }
        public int MessageTemplateID { get; set; }
        public int MessageID { get; set; }

        public bool createTemplate( ref int _iMessageTemplateID, ref int _iStaticMessageOption, ref int _iProcessTemplateID, out string _sError, ref int _iMessageID ) {
            //bool _b = createProcess();
            this.MessageID = _iMessageID++;
            bool _b = createResponseMessageTemplates( ref _iProcessTemplateID, ref _iStaticMessageOption, ref _iMessageTemplateID );
            //_b = createProcessTemplates( ref _iStaticMessageOption, ref _iMessageTemplateID, ref _iProcessTemplateID );
            return g_UnitOfWork.commit( out _sError );
        }

        public TmplDataWelcome() {
        }

        //public bool createProcess() {
        //    try {
        //        g_UnitOfWork.Processes.insertOrUpdate( new Process {
        //            ProcessID = ( int )EnumAppProcessMain.welcome_message,
        //            Name = "Welcome message"
        //        } );
        //        return g_UnitOfWork.commit( out string _sError );
        //    } catch (Exception _Ex) { }
        //    return false;
        //}

        public bool createResponseMessageTemplates( ref int _iProcessTemplateID, ref int _iStaticMessageOptions, ref int _iMessageTemplateID ) {
            try {
                ProcessTemplate _Process = new ProcessTemplate {
                    ProcessTemplateID = _iProcessTemplateID,
                    OnSuccessMessageOthers = false,
                    ProcessID = ( int )EnumAppProcessMain.welcome_message,
                    SubProcessID = ( int )EnumWelcomeSub.greeting_and_menu,
                };
                //Failure Message(s)
                this.MessageID++;
                g_UnitOfWork.ResponseMessageTemplates.insertOrUpdate( new ResponseMessageTemplate {
                    ResponseMessageTemplateID = ++_iMessageTemplateID,
                    MessageID = this.MessageID,
                    PrefixText = "Ooops. I didn't get that. Please retry.",
                    AppUserTypeID = 0,
                    HasBackOption = false,
                    Description = "Invalid user input on welcome page.",
                    HasStaticOptions = true,
                    HasDynamicOptions = false,
                    MessageTypeID = ( int )EnumMessageType.welcome,
                    StatusID = ( int )EnumResponseMessageTemplateStatus.active
                } );
                g_UnitOfWork.commit( out string _sError );
                _Process.FailedMessageID = this.MessageID;
                //Success Message(s)
                this.MessageID++;
                string _sGreeting = "Hi {Nickname}. \nWhat do you want to do?";
                #region Super admin main options
                g_UnitOfWork.ResponseMessageTemplates.insertOrUpdate( new ResponseMessageTemplate {
                    ResponseMessageTemplateID = ++_iMessageTemplateID,
                    MessageID = this.MessageID,
                    PrefixText = _sGreeting,
                    AppUserTypeID = ( int )EnumAppUserType.super_admin,
                    HasBackOption = false,
                    Description = "Super admin main options.",
                    HasStaticOptions = true,
                    HasDynamicOptions = false,
                    MessageTypeID = ( int )EnumMessageType.welcome,
                    StatusID = ( int )EnumResponseMessageTemplateStatus.active
                } );
                g_UnitOfWork.commit( out _sError );
                createStaticMessageOptionsSuperAdmin( ref _iStaticMessageOptions, ref _iMessageTemplateID );
                #endregion

                #region Super Business Admin
                g_UnitOfWork.ResponseMessageTemplates.insertOrUpdate( new ResponseMessageTemplate {
                    ResponseMessageTemplateID = ++_iMessageTemplateID,
                    MessageID = this.MessageID,
                    PrefixText = _sGreeting,
                    AppUserTypeID = ( int )EnumAppUserType.business_admin,
                    HasBackOption = false,
                    Description = "Business admin main options.",
                    HasStaticOptions = true,
                    HasDynamicOptions = false,
                    MessageTypeID = ( int )EnumMessageType.welcome,
                    StatusID = ( int )EnumResponseMessageTemplateStatus.active
                } );
                g_UnitOfWork.commit( out _sError );
                createStaticMessageOptionsBusinessAdmin( ref _iStaticMessageOptions, ref _iMessageTemplateID );
                #endregion

                #region Parcel Sender
                g_UnitOfWork.ResponseMessageTemplates.insertOrUpdate( new ResponseMessageTemplate {
                    ResponseMessageTemplateID = ++_iMessageTemplateID,
                    MessageID = this.MessageID,
                    PrefixText = "Hi {Nickname}.\n",
                    AppUserTypeID = ( int )EnumAppUserType.parcel_sender,
                    HasBackOption = false,
                    Description = "Parcel sender admin main options.",
                    HasStaticOptions = true,
                    HasDynamicOptions = false,
                    SuffixText = "\nTo track your order, please enter the trip code.",
                    MessageTypeID = ( int )EnumMessageType.welcome,
                    StatusID = ( int )EnumResponseMessageTemplateStatus.active
                } );
                g_UnitOfWork.commit( out _sError );
                #endregion

                #region Parcel Receiver
                g_UnitOfWork.ResponseMessageTemplates.insertOrUpdate( new ResponseMessageTemplate {
                    ResponseMessageTemplateID = ++_iMessageTemplateID,
                    MessageID = this.MessageID,
                    PrefixText = "Hi {Nickname}.\n",
                    AppUserTypeID = ( int )EnumAppUserType.parcel_receiver,
                    HasBackOption = false,
                    Description = "Parcel receiver admin main options.",
                    HasStaticOptions = true,
                    HasDynamicOptions = false,
                    SuffixText = "\nTo track your order, please enter the trip code.",
                    MessageTypeID = ( int )EnumMessageType.welcome,
                    StatusID = ( int )EnumResponseMessageTemplateStatus.active
                } );
                g_UnitOfWork.commit( out _sError );
                #endregion

                #region Guest
                g_UnitOfWork.ResponseMessageTemplates.insertOrUpdate( new ResponseMessageTemplate {
                    ResponseMessageTemplateID = ++_iMessageTemplateID,
                    MessageID = this.MessageID,
                    PrefixText = "Hi there.\nWhat do you want to do?",
                    AppUserTypeID = ( int )EnumAppUserType.guest,
                    HasBackOption = false,
                    Description = "Guest main options.",
                    HasStaticOptions = true,
                    SuffixText = "\nTo learn more about this application or for any other information, please contact us at " + Constant.COMPANY_EMAIL,
                    HasDynamicOptions = false,
                    MessageTypeID = ( int )EnumMessageType.welcome,
                    StatusID = ( int )EnumResponseMessageTemplateStatus.active
                } );
                g_UnitOfWork.commit( out _sError );
                #endregion
                _Process.SuccessMessageID = this.MessageID;
                g_UnitOfWork.ProcessTemplates.insertOrUpdate( _Process );
                return g_UnitOfWork.commit( out _sError );
            } catch { }
            return false;
        }

        public bool createStaticMessageOptionsSuperAdmin( ref int _iStaticMessageOptions, ref int _iMessageTemplateID ) {
            int _iOptionValueID = 0;
            try {
                g_UnitOfWork.StaticMessageOptions.insertOrUpdate( new StaticMessageOption {
                    StaticMessageOptionID = ++_iStaticMessageOptions,
                    MessageTemplateID = _iMessageTemplateID,
                    Text = "Register a business",
                    OptionNextProcessID = ( int )EnumAppProcessMain.super_admin_register_business,
                    OptionNextSubProcessID = ( int )EnumSuperAdminRegisterBusinessSub.add_name,
                    OptionValueID = ( ++_iOptionValueID ).ToString()
                } );
                //Add logic to update a business
            } catch { }
            return false;
        }

        public bool createStaticMessageOptionsBusinessAdmin( ref int _iStaticMessageOptions, ref int _iMessageTemplateID ) {
            int _iOptionValueID = 0;
            try {
                g_UnitOfWork.StaticMessageOptions.insertOrUpdate( new StaticMessageOption {
                    StaticMessageOptionID = ++_iStaticMessageOptions,
                    MessageTemplateID = _iMessageTemplateID,
                    Text = "Create new trip",
                    OptionNextProcessID = ( int )EnumAppProcessMain.business_admin_create_trip,
                    OptionNextSubProcessID = ( int )EnumBusinessAdminCreateTripSub.choose_driver,
                    OptionValueID = ( ++_iOptionValueID ).ToString()
                } );
                g_UnitOfWork.StaticMessageOptions.insertOrUpdate( new StaticMessageOption {
                    StaticMessageOptionID = ++_iStaticMessageOptions,
                    MessageTemplateID = _iMessageTemplateID,
                    Text = "Create new vehicle",
                    OptionNextProcessID = ( int )EnumAppProcessMain.business_admin_add_vehicle,
                    OptionNextSubProcessID = ( int )EnumBusinessAdminAddVehicleSub.add_registration,
                    OptionValueID = ( ++_iOptionValueID ).ToString()
                } );
                g_UnitOfWork.StaticMessageOptions.insertOrUpdate( new StaticMessageOption {
                    StaticMessageOptionID = ++_iStaticMessageOptions,
                    MessageTemplateID = _iMessageTemplateID,
                    Text = "Create new driver",
                    OptionNextProcessID = ( int )EnumAppProcessMain.business_admin_add_transporter,
                    OptionNextSubProcessID = ( int )EnumBusinessAdminAddTransporterSub.enter_name,
                    OptionValueID = ( ++_iOptionValueID ).ToString()
                } );
            } catch { }
            return false;
        }
    }
}