﻿using System;

namespace CargoBot.Models {
    public class MessageRecipient {
        public int UserTypeID { get; set; }
        public string PhoneNumber { get; set; }
        public Guid AppUserID { get; set; }
    }
}