﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CargoBot.Repositories;

namespace CargoBot.Models {
    [Table( SqlTable.LU_BUSINESS_STATUS )]
    public class BusinessStatus : HasCreatedAndLastModified {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int BusinessStatusID { get; set; }

        [Required, MaxLength( 100 )]
        public string Name { get; set; }

        public static bool initData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.BusinessStatuses.insertOrUpdate( new BusinessStatus {
                    BusinessStatusID = ( int )EnumBusinessStatus.active,
                    Name = "Active",
                } );
                g_UnitOfWork.BusinessStatuses.insertOrUpdate( new BusinessStatus {
                    BusinessStatusID = ( int )EnumBusinessStatus.suspended,
                    Name = "Suspended",
                } );
                g_UnitOfWork.BusinessStatuses.insertOrUpdate( new BusinessStatus {
                    BusinessStatusID = ( int )EnumBusinessStatus.unpaid,
                    Name = "Unpaid",
                } );
                return g_UnitOfWork.commit( out string _sError );
            } catch ( Exception ) {
            }
            return false;
        }

        public static bool clearData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.BusinessStatuses.deleteAll();
                return g_UnitOfWork.commit( out string _sError );
            } catch {
            }
            return false;
        }
    }
}