﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CargoBot.Models {
    [Table( SqlTable.TBL_PARCEL_IMAGE )]
    public class ParcelImage : HasCreatedAndLastModified {
        [Key] public Guid ParcelImageID { get; set; }
        [Required] public string ImageUrl { get; set; }

        [ForeignKey( "Parcel" )]
        public Guid ParcelID { get; set; }
        public virtual Parcel Parcel { get; set; }
    }
}