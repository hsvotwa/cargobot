﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CargoBot.Models {
    [Table( SqlTable.TBL_VEHICLE )]
    public class Vehicle : HasCreatedAndLastModified {
        [Key] public Guid VehicleID { get; set; }
        [Required] public string RegNo { get; set; }
        [Required] public string Description { get; set; }

        [ForeignKey( "Business" )]
        public Guid BusinessID { get; set; }
        public virtual Business Business { get; set; }
    }
}