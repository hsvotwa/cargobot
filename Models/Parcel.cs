﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CargoBot.Models {
    [Table( SqlTable.TBL_PARCEL )]
    public class Parcel : HasCreatedAndLastModified {
        [Key] public Guid ParcelID { get; set; }
        [Required] public string ParcelCode { get; set; }
        [Required] public string Description { get; set; }

        [ForeignKey( "SenderAppUser" )]
        public Guid SenderAppUserID { get; set; }
        public virtual AppUser SenderAppUser { get; set; }

        [ForeignKey( "RecipientAppUser" )]
        public Guid RecipientAppUserID { get; set; }
        public virtual AppUser RecipientAppUser { get; set; }

        [ForeignKey( "ParcelStatus" )]
        public int ParcelStatusID { get; set; }
        public virtual ParcelStatus ParcelStatus { get; set; }

        [ForeignKey( "Trip" )]
        public Guid TripID { get; set; }
        public virtual Trip Trip { get; set; }
    }
}