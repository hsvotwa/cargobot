﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CargoBot.Models {
    [Table( SqlTable.TBL_PROCESS_TEMPLATE )]
    public class ProcessTemplate {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int ProcessTemplateID { get; set; }

        [ForeignKey( "Process" )]
        public int ProcessID { get; set; }
        public virtual Process Process { get; set; }

        //[ForeignKey( "SubProcess" )]
        public int SubProcessID { get; set; }
        //public virtual Process SubProcess { get; set; }

        //[ForeignKey( "SuccessMessageTemplate" )]
        public int? SuccessMessageID { get; set; }
        //public virtual ResponseMessageTemplate SuccessResponseMessageTemplate { get; set; }

        //[ForeignKey( "FailedMessageTemplate" )]
        public int? FailedMessageID { get; set; }
        //public virtual ResponseMessageTemplate FailedResponseMessageTemplate { get; set; }

        public bool OnSuccessMessageOthers { get; set; }
    }
}