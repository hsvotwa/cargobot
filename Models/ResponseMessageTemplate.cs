﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CargoBot.Models;
using CargoBot.Repositories;

namespace CargoBot.Models {
    [Table( SqlTable.TBL_RESPONSE_MESSAGE_TEMPLATE )]
    public class ResponseMessageTemplate {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int ResponseMessageTemplateID { get; set; }

        //[ForeignKey( "Message" )]
        public int MessageID { get; set; }
        //public virtual Message Message { get; set; }

        public string PrefixText { get; set; }
        [Required] public bool HasStaticOptions { get; set; }
        [Required] public bool HasDynamicOptions { get; set; }

        [ForeignKey( "DynamicListType" )]
        public int? DynamicListTypeID { get; set; }
        public virtual DynamicListType DynamicListType { get; set; }

        public string SuffixText { get; set; }
        [Required] public bool HasBackOption { get; set; }
        public string BackOptionText { get; set; }

        [ForeignKey( "BackOptionProcess" )]
        public int? BackOptionProcessID { get; set; }
        public virtual Process BackOptionProcess { get; set; }

        [Required] public string Description { get; set; }

        [ForeignKey( "ForUserType" )]
        public int AppUserTypeID { get; set; }
        public virtual AppUserType ForUserType { get; set; }

        [ForeignKey( "MessageType" )]
        public int? MessageTypeID { get; set; }
        public virtual MessageType MessageType { get; set; }

        [ForeignKey( "ResponseMessageTemplateStatus" )]
        public int? StatusID { get; set; }
        public virtual ResponseMessageTemplateStatus ResponseMessageTemplateStatus { get; set; }

        //[Required] public string ExpectedFeedback { get; set; }

        private UnitOfWork g_UnitOfWork = new UnitOfWork( new CargoBotContext() );
    }
}