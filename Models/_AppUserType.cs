﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CargoBot.Repositories;

namespace CargoBot.Models {
    [Table( SqlTable.LU_APP_USER_TYPE )]
    public class AppUserType : HasCreatedAndLastModified {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int AppUserTypeID { get; set; }

        [Required, MaxLength( 100 )]
        public string Name { get; set; }

        public static bool initData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.AppUserTypes.insertOrUpdate( new AppUserType {
                    AppUserTypeID = ( int )EnumAppUserType.message_sender,
                    Name = "Message sender",
                } );
                g_UnitOfWork.AppUserTypes.insertOrUpdate( new AppUserType {
                    AppUserTypeID = ( int )EnumAppUserType.guest,
                    Name = "Guest",
                } );
                g_UnitOfWork.AppUserTypes.insertOrUpdate( new AppUserType {
                    AppUserTypeID = ( int )EnumAppUserType.parcel_sender,
                    Name = "Parcel Sender",
                } );
                g_UnitOfWork.AppUserTypes.insertOrUpdate( new AppUserType {
                    AppUserTypeID = ( int )EnumAppUserType.parcel_receiver,
                    Name = "Parcel Receiver",
                } );
                g_UnitOfWork.AppUserTypes.insertOrUpdate( new AppUserType {
                    AppUserTypeID = ( int )EnumAppUserType.business_admin,
                    Name = "Business Admin",
                } );
                g_UnitOfWork.AppUserTypes.insertOrUpdate( new AppUserType {
                    AppUserTypeID = ( int )EnumAppUserType.transporter_or_driver,
                    Name = "Transporter",
                } );
                g_UnitOfWork.AppUserTypes.insertOrUpdate( new AppUserType {
                    AppUserTypeID = ( int )EnumAppUserType.super_admin,
                    Name = "Super Admin",
                } );
                return g_UnitOfWork.commit( out string _sError );
            } catch ( Exception _Ex ) {
            }
            return false;
        }

        public static bool clearData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.AppUserTypes.deleteAll();
                return g_UnitOfWork.commit( out string _sError );
            } catch {
            }
            return false;
        }
    }
}