﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CargoBot.Repositories;

namespace CargoBot.Models
{
    [Table(SqlTable.LU_DYNAMIC_LIST_TYPE)]
    public class DynamicListType : HasCreatedAndLastModified
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DynamicListTypeID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        public static bool initData(UnitOfWork g_UnitOfWork)
        {
            try
            {
                g_UnitOfWork.DynamicListTypes.insertOrUpdate(new DynamicListType
                {
                    DynamicListTypeID = (int)EnumDynamicListType.drivers,
                    Name = "Drivers"
                });
                g_UnitOfWork.DynamicListTypes.insertOrUpdate(new DynamicListType
                {
                    DynamicListTypeID = (int)EnumDynamicListType.vehicles,
                    Name = "Vehicles"
                });
                g_UnitOfWork.DynamicListTypes.insertOrUpdate(new DynamicListType
                {
                    DynamicListTypeID = (int)EnumDynamicListType.parcel_receivers,
                    Name = "Parcel receivers"
                });
                g_UnitOfWork.DynamicListTypes.insertOrUpdate(new DynamicListType
                {
                    DynamicListTypeID = (int)EnumDynamicListType.route_locations,
                    Name = "Route locations"
                });
                g_UnitOfWork.DynamicListTypes.insertOrUpdate(new DynamicListType
                {
                    DynamicListTypeID = (int)EnumDynamicListType.trips,
                    Name = "Trips"
                });
                g_UnitOfWork.DynamicListTypes.insertOrUpdate(new DynamicListType
                {
                    DynamicListTypeID = (int)EnumDynamicListType.sender_parcels,
                    Name = "Sender Parcels"
                });
                g_UnitOfWork.DynamicListTypes.insertOrUpdate(new DynamicListType
                {
                    DynamicListTypeID = (int)EnumDynamicListType.receiver_parcels,
                    Name = "Receiver parcels"
                });
                return g_UnitOfWork.commit(out string _sError);
            }
            catch (Exception)
            {
            }
            return false;
        }

        public static bool clearData(UnitOfWork g_UnitOfWork)
        {
            try
            {
                g_UnitOfWork.DynamicListTypes.deleteAll();
                return g_UnitOfWork.commit(out string _sError);
            }
            catch
            {
            }
            return false;
        }
    }
}