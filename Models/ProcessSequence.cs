﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CargoBot.Models {
    [Table( SqlTable.TBL_PROCESS_SEQUENCE )]
    public class ProcessSequence {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )] 
        public int ProcessSequenceID { get; set; }

        [ForeignKey( "Process" )]
        public int ProcessID { get; set; }
        public virtual Process Process { get; set; }

        [ForeignKey( "NextProcess" )]
        public int NextProcessID { get; set; }
        public virtual Process NextProcess { get; set; }
    }
}