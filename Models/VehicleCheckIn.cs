﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CargoBot.Models {
    [Table( SqlTable.TBL_VEHICLE_CHECKIN )]
    public class VehicleCheckIn : HasCreatedAndLastModified {
        [Key] public Guid VehicleCheckInID { get; set; }
        public DateTime DateAndTime { get; set; }
        [Required] public string Location { get; set; }

        [ForeignKey( "AppUserDriver" )]
        public Guid AppUserID { get; set; }
        public virtual AppUser AppUserDriver { get; set; }

        [ForeignKey( "Trip" )]
        public Guid TripID { get; set; }
        public virtual Trip Trip { get; set; }
    }
}