﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CargoBot.Models {
    [Table( SqlTable.TBL_BUSINESS )]
    public class Business : HasCreatedAndLastModified {
        [Key] public Guid BusinessID { get; set; }
        [Required] public string BusinessCode { get; set; }
        [Required] public string Name { get; set; }
        [Required] public string Phone { get; set; }
        public string Address { get; set; }

        [ForeignKey( "BusinessStatus" )]
        public int StatusID { get; set; }
        public virtual BusinessStatus BusinessStatus { get; set; }
    }
}