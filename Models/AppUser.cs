﻿using CargoBot.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CargoBot.Models {
    [Table( SqlTable.TBL_APP_USER )]
    public class AppUser : HasCreatedAndLastModified {
        [Key] public Guid AppUserID { get; set; }
        [Required] public string Name { get; set; }
        public string Nickname { get; set; }
        [Required] public string Phone { get; set; }
        public string LastKnownLocation { get; set; }

        public AppUser() {
        }

        public static List<AppUser> SUPER_ADMINS {
            get {
                return new List<AppUser> { new AppUser {
                    AppUserID = Guid.Parse("6615982d-8674-46ec-9f83-af229b7d3710"),
                    Nickname = "Hope",
                    Phone = "+264810365066",
                    LastKnownLocation = "Osona Village, Otzojonjupa Region, Namibia",
                    Name = "Hope Svotwa"
                } ,
                    new AppUser {
                    AppUserID = Guid.Parse( "189bbb9f-4aff-492f-8b7e-272b1cbca1a7" ),
                    Nickname = "Jimmy",
                    Phone = "+264813215090",
                    LastKnownLocation = "Johanesburg, South Africa",
                    Name = "Courage Svotwa"
                } };
            }
        }

        public static bool setSuperAdmins() {
            try {
                UnitOfWork _UnitOfWork = new UnitOfWork( new CargoBotContext() );
                foreach ( AppUser _SuperAdmin in SUPER_ADMINS ) {
                    _UnitOfWork.AppUsers.insertOrUpdate( _SuperAdmin );
                }
                return _UnitOfWork.commit( out string _sErrorMessage );
            } catch { }
            return false;
        }
    }
}