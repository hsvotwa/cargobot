﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CargoBot.Repositories;

namespace CargoBot.Models {
    [Table( SqlTable.LU_BUSINESS_ADMIN_STATUS )]
    public class BusinessAdminStatus : HasCreatedAndLastModified {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int BusinessAdminStatusID { get; set; }

        [Required, MaxLength( 100 )]
        public string Name { get; set; }

        public static bool initData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.BusinessAdminStatuses.insertOrUpdate( new BusinessAdminStatus {
                    BusinessAdminStatusID = ( int )EnumBusinessAdminStatus.active,
                    Name = "Active",
                } );
                g_UnitOfWork.BusinessAdminStatuses.insertOrUpdate( new BusinessAdminStatus {
                    BusinessAdminStatusID = ( int )EnumBusinessAdminStatus.invited_to_chat,
                    Name = "Invited to chat",
                } );
                g_UnitOfWork.BusinessAdminStatuses.insertOrUpdate( new BusinessAdminStatus {
                    BusinessAdminStatusID = ( int )EnumBusinessAdminStatus.deleted,
                    Name = "Deleted",
                } );
                return g_UnitOfWork.commit(out string _sError);
            } catch ( Exception ) {
            }
            return false;
        }

        public static bool clearData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.BusinessAdminStatuses.deleteAll();
                return g_UnitOfWork.commit( out string _sError );
            } catch {
            }
            return false;
        }
    }
}