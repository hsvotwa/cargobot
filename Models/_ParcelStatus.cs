﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CargoBot.Repositories;

namespace CargoBot.Models {
    [Table( SqlTable.LU_PARCEL_STATUS )]
    public class ParcelStatus : HasCreatedAndLastModified {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int ParcelStatusID { get; set; }

        [Required, MaxLength( 100 )]
        public string Name { get; set; }

        public static bool initData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.ParcelStatuses.insertOrUpdate( new ParcelStatus {
                    ParcelStatusID = ( int )EnumParcelStatus.enroute,
                    Name = "Enroute",
                } );
                g_UnitOfWork.ParcelStatuses.insertOrUpdate( new ParcelStatus {
                    ParcelStatusID = ( int )EnumParcelStatus.pending_collection,
                    Name = "Pending Collection",
                } );
                g_UnitOfWork.ParcelStatuses.insertOrUpdate( new ParcelStatus {
                    ParcelStatusID = ( int )EnumParcelStatus.collected,
                    Name = "Collected",
                } );
                return g_UnitOfWork.commit( out string _sError );
            } catch ( Exception ) {
            }
            return false;
        }

        public static bool clearData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.ParcelStatuses.deleteAll();
                return g_UnitOfWork.commit( out string _sError );
            } catch {
            }
            return false;
        }
    }
}