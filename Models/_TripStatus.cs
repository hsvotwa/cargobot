﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CargoBot.Repositories;

namespace CargoBot.Models {
    [Table( SqlTable.LU_TRIP_STATUS )]
    public class TripStatus : HasCreatedAndLastModified {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int TripStatusID { get; set; }

        [Required, MaxLength( 100 )]
        public string Name { get; set; }

        public static bool initData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.TripStatuses.insertOrUpdate( new TripStatus {
                    TripStatusID = ( int )EnumTripStatus.loading,
                    Name = "Loading",
                } );
                g_UnitOfWork.TripStatuses.insertOrUpdate( new TripStatus {
                    TripStatusID = ( int )EnumTripStatus.departed,
                    Name = "Departed",
                } );
                g_UnitOfWork.TripStatuses.insertOrUpdate( new TripStatus {
                    TripStatusID = ( int )EnumTripStatus.arrived,
                    Name = "Arrived",
                } );
                return g_UnitOfWork.commit( out string _sError );
            } catch ( Exception ) {
            }
            return false;
        }

        public static bool clearData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.TripStatuses.deleteAll();
                return g_UnitOfWork.commit( out string _sError );
            } catch {
            }
            return false;
        }
    }
}