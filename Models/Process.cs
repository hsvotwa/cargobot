﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CargoBot.Repositories;

namespace CargoBot.Models {
    [Table( SqlTable.TBL_PROCESS )]
    public class Process {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int ProcessID { get; set; }
        public string Name { get; set; }

        #region InitData
        private UnitOfWork g_UnitOfWork = new UnitOfWork( new CargoBotContext() );
        #endregion
    }
}