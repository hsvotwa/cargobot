﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CargoBot.Models {
    [Table( SqlTable.TBL_TRIP )]
    public class Trip : HasCreatedAndLastModified {
        [Key] public Guid TripID { get; set; }
        [Required] public string TripCode { get; set; }
        public DateTime Date { get; set; }

        [ForeignKey( "AppUserDriver" )]
        public Guid AppUserID { get; set; }
        public virtual AppUser AppUserDriver { get; set; }

        [ForeignKey( "Business" )]
        public Guid BusinessID { get; set; }
        public virtual Business Business { get; set; }

        [ForeignKey( "TripStatus" )]
        public int TripStatusID { get; set; }
        public virtual TripStatus TripStatus { get; set; }
    }
}