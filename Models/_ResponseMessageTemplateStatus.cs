﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CargoBot.Repositories;

namespace CargoBot.Models
{
    [Table(SqlTable.LU_RESPONSE_MESSAGE_TEMPLATE_STATUS)]
    public class ResponseMessageTemplateStatus
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StatusID { get; set; }

        [Required, MaxLength(100)]
        public string Name { get; set; }

        public static bool initData(UnitOfWork g_UnitOfWork)
        {
            try
            {
                g_UnitOfWork.ResponseMessageTemplateStatuses.insertOrUpdate(new ResponseMessageTemplateStatus
                {
                    StatusID = (int)EnumResponseMessageTemplateStatus.active,
                    Name = "Active"
                });
                g_UnitOfWork.ResponseMessageTemplateStatuses.insertOrUpdate(new ResponseMessageTemplateStatus
                {
                    StatusID = (int)EnumResponseMessageTemplateStatus.inactive,
                    Name = "Inactive"
                });
                g_UnitOfWork.ResponseMessageTemplateStatuses.insertOrUpdate(new ResponseMessageTemplateStatus
                {
                    StatusID = (int)EnumResponseMessageTemplateStatus.replaced,
                    Name = "Replaced"
                });
                return g_UnitOfWork.commit(out string _sError);
            }
            catch (Exception)
            {
            }
            return false;
        }

        public static bool clearData(UnitOfWork g_UnitOfWork)
        {
            try
            {
                g_UnitOfWork.ResponseMessageTemplateStatuses.deleteAll();
                return g_UnitOfWork.commit(out string _sError);
            }
            catch
            {
            }
            return false;
        }
    }
}