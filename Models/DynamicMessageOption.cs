﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CargoBot.Models {
    [Table( SqlTable.TBL_DYNAMIC_MESSAGE_OPTION )]
    public class DynamicMessageOption {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int MessageOptionID { get; set; }

        [Required]
        [ForeignKey( "MessageTemplate" )]
        public int MessageTemplateID { get; set; }
        public virtual ResponseMessageTemplate MessageTemplate { get; set; }

        [Required] public string OptionValueID { get; set; }
        public string RelatedTableKey { get; set; }
        [Required] public string Text { get; set; }
    }
}