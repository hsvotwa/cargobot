﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CargoBot.Repositories;

namespace CargoBot.Models {
    [Table( SqlTable.LU_MESSAGE_TYPE )]
    public class MessageType {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.None )]
        public int MessageTypeID { get; set; }

        [Required, MaxLength( 100 )]
        public string Name { get; set; }

        public static bool initData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.MessageTypes.insertOrUpdate( new MessageType {
                    MessageTypeID = ( int )EnumMessageType.welcome,
                    Name = "Welcome"
                } );
                g_UnitOfWork.MessageTypes.insertOrUpdate( new MessageType {
                    MessageTypeID = ( int )EnumMessageType.question,
                    Name = "Question"
                } );
                g_UnitOfWork.MessageTypes.insertOrUpdate( new MessageType {
                    MessageTypeID = ( int )EnumMessageType.invalid_input,
                    Name = "Invalid input"
                } );
                g_UnitOfWork.MessageTypes.insertOrUpdate( new MessageType {
                    MessageTypeID = ( int )EnumMessageType.error,
                    Name = "Error"
                } );
                g_UnitOfWork.MessageTypes.insertOrUpdate( new MessageType {
                    MessageTypeID = ( int )EnumMessageType.success,
                    Name = "Success"
                } );
                return g_UnitOfWork.commit( out string _sError );
            } catch ( Exception ) {
            }
            return false;
        }

        public static bool clearData( UnitOfWork g_UnitOfWork ) {
            try {
                g_UnitOfWork.MessageTypes.deleteAll();
                return g_UnitOfWork.commit( out string _sError );
            } catch {
            }
            return false;
        }
    }
}