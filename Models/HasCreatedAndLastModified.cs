﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CargoBot.Models {
    public class HasCreatedAndLastModified {
        [Column( TypeName = "DateTime2" )]
        [JsonIgnore]
        public DateTime Created { get; set; }

        [Column( TypeName = "DateTime2" )]
        [JsonIgnore]
        public DateTime LastModified { get; set; }
    }
}