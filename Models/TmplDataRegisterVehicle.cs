﻿using CargoBot.Repositories;

namespace CargoBot.Models
{
    public class TmplDataRegisterVehicle
    {
        private UnitOfWork g_UnitOfWork = new UnitOfWork(new CargoBotContext());
        public int StaticMessageOptionID { get; set; }
        public int MessageTemplateID { get; set; }
        public int MessageID { get; set; }

        public bool createTemplate(ref int _iMessageTemplateID, ref int _iStaticMessageOption, ref int _iProcessTemplateID, out string _sError, ref int _iMessageID)
        {
            //bool _b = createProcess();
            this.MessageID = _iMessageID++;
            bool _b = createResponseMessageTemplates(ref _iProcessTemplateID, ref _iStaticMessageOption, ref _iMessageTemplateID);
            //_b = createProcessTemplates( ref _iStaticMessageOption, ref _iMessageTemplateID, ref _iProcessTemplateID );
            return g_UnitOfWork.commit(out _sError);
        }

        public TmplDataRegisterVehicle()
        {
        }

        public bool createResponseMessageTemplates(ref int _iProcessTemplateID, ref int _iStaticMessageOptions, ref int _iMessageTemplateID)
        {
            try
            {
                ProcessTemplate _Process = new ProcessTemplate
                {
                    ProcessTemplateID = _iProcessTemplateID,
                    OnSuccessMessageOthers = false,
                    ProcessID = (int)EnumAppProcessMain.business_admin_add_vehicle,
                    SubProcessID = (int)EnumBusinessAdminAddVehicleSub.add_registration,
                };
                //Failure Message(s)
                this.MessageID++;
                g_UnitOfWork.ResponseMessageTemplates.insertOrUpdate(new ResponseMessageTemplate
                {
                    ResponseMessageTemplateID = ++_iMessageTemplateID,
                    MessageID = this.MessageID,
                    PrefixText = "Invalid registration number. Please retry.",
                    AppUserTypeID = 0,
                    HasBackOption = false,
                    Description = "Invalid user input on vehicle reg number.",
                    HasStaticOptions = true,
                    HasDynamicOptions = false,
                    MessageTypeID = (int)EnumMessageType.invalid_input,
                    StatusID = (int)EnumResponseMessageTemplateStatus.active
                });
                g_UnitOfWork.commit(out string _sError);
                _Process.FailedMessageID = this.MessageID;
                //Question Message(s)
                this.MessageID++;

                #region Super Business Admin
                g_UnitOfWork.ResponseMessageTemplates.insertOrUpdate(new ResponseMessageTemplate
                {
                    ResponseMessageTemplateID = ++_iMessageTemplateID,
                    MessageID = this.MessageID,
                    PrefixText = "Enter vehicle registration number",
                    AppUserTypeID = (int)EnumAppUserType.business_admin,
                    HasBackOption = false,
                    Description = "Vehicle reg number question.",
                    HasStaticOptions = false,
                    HasDynamicOptions = false,
                    MessageTypeID = (int)EnumMessageType.question,
                    StatusID = (int)EnumResponseMessageTemplateStatus.active
                });
                g_UnitOfWork.commit(out _sError);

                g_UnitOfWork.ResponseMessageTemplates.insertOrUpdate(new ResponseMessageTemplate
                {
                    ResponseMessageTemplateID = ++_iMessageTemplateID,
                    MessageID = this.MessageID,
                    PrefixText = "Enter vehicle short description",
                    AppUserTypeID = (int)EnumAppUserType.business_admin,
                    HasBackOption = false,
                    Description = "Vehicle description question.",
                    HasStaticOptions = false,
                    HasDynamicOptions = false,
                    MessageTypeID = (int)EnumMessageType.question,
                    StatusID = (int)EnumResponseMessageTemplateStatus.active
                });
                g_UnitOfWork.commit(out _sError);

                //Success Message(s)
                g_UnitOfWork.ResponseMessageTemplates.insertOrUpdate(new ResponseMessageTemplate
                {
                    ResponseMessageTemplateID = ++_iMessageTemplateID,
                    MessageID = this.MessageID,
                    PrefixText = "Vehicle regisration succesful.",
                    AppUserTypeID = (int)EnumAppUserType.business_admin,
                    HasBackOption = false,
                    Description = "Vehicle reg success.",
                    HasStaticOptions = false,
                    HasDynamicOptions = true,
                    MessageTypeID = (int)EnumMessageType.success,
                    StatusID = (int)EnumResponseMessageTemplateStatus.active
                });
                g_UnitOfWork.commit(out _sError);
                #endregion

                _Process.SuccessMessageID = this.MessageID;
                g_UnitOfWork.ProcessTemplates.insertOrUpdate(_Process);
                return g_UnitOfWork.commit(out _sError);
            }
            catch { }
            return false;
        }
    }
}