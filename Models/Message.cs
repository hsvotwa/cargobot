﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace CargoBot.Models {
    [Table( SqlTable.TBL_MESSAGE )]
    public class Message : HasCreatedAndLastModified {
        [Key] public int MessageID { get; set; } //Auto-increment
        [Required] public string TwilioMessageID { get; set; }
        public string Nickname { get; set; }
        [Required] public string SenderPhoneNumber { get; set; }
        public DateTime DateAndTime { get; set; }
        public bool IsValid { get; set; }
        public bool IsTemplateResponse { get; set; }
        public double Cost { get; set; }
        public string BodyReceived { get; set; }
        public string BodySent { get; set; }

        [ForeignKey( "Process" )]
        public int ProcessID { get; set; }
        public virtual Process Process { get; set; }

        //[ForeignKey( "SubProcess" )]
        public int SubProcessID { get; set; }
        //public virtual SubProcess SubProcess { get; set; }

        [ForeignKey( "ResponseMessageTemplate" )]
        public int ResponseMessageTemplateID { get; set; }
        public virtual ResponseMessageTemplate ResponseMessageTemplate { get; set; }

        [ForeignKey( "Trip" )]
        public Guid? TripID { get; set; }
        public virtual Trip Trip { get; set; }

        [ForeignKey( "Parcel" )]
        public Guid? ParcelID { get; set; }
        public virtual Parcel Parcel { get; set; }

        [ForeignKey( "AppUserType" )]
        public int AppUserTypeID { get; set; }
        public virtual AppUserType AppUserType { get; set; }


        //public string sendWhatsApp( out decimal _dPriceBilled ) {
        //    string _sAccountSid = "ACa7dca9790457aea4ecfcd5e9dcd0d67f";
        //    string _sAuthToken = "3aedc465ef348a67128c32729efc9dbf";
        //    TwilioClient.Init( _sAccountSid, _sAuthToken );
        //    CreateMessageOptions _CreateMessageOption = new CreateMessageOptions( new PhoneNumber( "whatsapp:+264810365066" ) );
        //    _CreateMessageOption.From = new PhoneNumber( "whatsapp:+14155238886" );
        //    _CreateMessageOption.Body = "Your appointment is coming up on July 21 at 3PM";
        //    MessageResource _MessageResource = MessageResource.Create( _CreateMessageOption );
        //    if ( !decimal.TryParse( _MessageResource.Price, out _dPriceBilled ) ) {
        //        _dPriceBilled = 0;
        //    }
        //    return _MessageResource.Sid;
            //// Typical response
            //            {
            //              "account_sid": "ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
            //              "api_version": "2010-04-01",
            //              "body": "Hello there!",
            //              "date_created": "Thu, 30 Jul 2015 20:12:31 +0000",
            //              "date_sent": "Thu, 30 Jul 2015 20:12:33 +0000",
            //              "date_updated": "Thu, 30 Jul 2015 20:12:33 +0000",
            //              "direction": "outbound-api",
            //              "error_code": null,
            //              "error_message": null,
            //              "from": "whatsapp:+14155238886",
            //              "messaging_service_sid": "MGXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
            //              "num_media": "0",
            //              "num_segments": "1",
            //              "price": null,
            //              "price_unit": null,
            //              "sid": "SMXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
            //              "status": "sent",
            //              "subresource_uris": {
            //                "media": "/2010-04-01/Accounts/ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/Messages/SMXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/Media.json"
            //              },
            //              "to": "whatsapp:+15005550006",
            //              "uri": "/2010-04-01/Accounts/ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/Messages/SMXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX.json"
            //}
        //}
    }
}