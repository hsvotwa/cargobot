﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CargoBot.Models {
    [Table( SqlTable.TBL_BUSINESS_ADMIN )]
    public class BusinessAdmin : HasCreatedAndLastModified {
        [Key] public Guid BusinessAdminID { get; set; }

        [ForeignKey( "BusinessAdminStatus" )]
        public int BusinessAdminStatusID { get; set; }
        public virtual BusinessAdminStatus BusinessAdminStatus { get; set; }

        [ForeignKey( "AppUser" )]
        public Guid AppUserID { get; set; }
        public virtual AppUser AppUser { get; set; }

        [ForeignKey( "Business" )]
        public Guid BusinessID { get; set; }
        public virtual Business Business { get; set; }
    }
}